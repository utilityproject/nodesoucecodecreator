var groupCourseCategorySectionMaster = require("../models/group_course_category_section_master");
var logger = require("../../utils/logger");
var responseCode = require("../../utils/response_code");

function updateGroupCourseCategorySection(requestObject, callback) {
	var updateObject = new Object();
	if(requestObject.categoryId)
		updateObject.categoryId = requestObject.categoryId;
	if(requestObject.sectionName)
		updateObject.sectionName = requestObject.sectionName;
	if(requestObject.refId)
		updateObject.refId = requestObject.refId;
	if(requestObject.displayOrder)
		updateObject.displayOrder = requestObject.displayOrder;
	if(requestObject.status)
		updateObject.status = requestObject.status;
	updateObject.updateAt = new Date();

	var responseObject = new Object();
	var query = {sectionId: requestObject.sectionId};
	groupCourseCategorySectionMaster.findOneAndUpdate(query, updateObject, function (error, data) {
		if (error) {
			logger.error(error);
			responseObject.responseCode = responseCode.MONGO_ERROR;
			callback(error, responseObject);
			return;
		}
		responseObject.responseCode = responseCode.SUCCESS;
		responseObject.responseData = data;
		callback(null, responseObject);
	});
}

module.exports = updateGroupCourseCategorySection;

// Unit Test Case
if (require.main === module) {
	var requestObject = new Object();
	requestObject.categoryId = "";
	requestObject.sectionName = "";
	requestObject.refId = "";
	requestObject.displayOrder = "";
	requestObject.status = "";
	console.log(requestObject);

	updateGroupCourseCategorySection(requestObject, function(error, responseObject) {
		console.log("Response Code - " + responseObject.responseCode);
		if (error)
			console.log("Error - " + error);
		else
			console.log("Response Data - " + responseObject.responseData);
	});
}