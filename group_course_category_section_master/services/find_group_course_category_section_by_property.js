var groupCourseCategorySectionMaster = require("../models/group_course_category_section_master");
var logger = require("../../utils/logger");
var responseCode = require("../../utils/response_code");

function findGroupCourseCategorySectionByProperty(requestObject, callback) {
	var query = groupCourseCategorySectionMaster.findOne({});
	if (typeof requestObject.sectionId !== "undefined" && requestObject.sectionId !== null)
		query.where("sectionId").equals(requestObject.sectionId);
	if (typeof requestObject.sectionName !== "undefined" && requestObject.sectionName !== null)
		query.where("sectionName").equals(requestObject.sectionName);

	var responseObject = new Object();
	query.exec(function (error, data) {
		if (error) {
			logger.error(error);
			responseObject.responseCode = responseCode.MONGO_ERROR;
			callback(error, responseObject);
			return;
		}
		responseObject.responseCode = responseCode.SUCCESS;
		responseObject.responseData = data;
		callback(null, responseObject);
	});
}

module.exports = findGroupCourseCategorySectionByProperty;

// Unit Test Case
if (require.main === module) {
	var requestObject = new Object();
	//requestObject.sectionId = "sectionId";
	//requestObject.sectionName = "sectionName";

	findGroupCourseCategorySectionByProperty(requestObject, function(error, responseObject) {
		console.log("Response Code - " + responseObject.responseCode);
		if (error)
			console.log("Error - " + error);
		else
			console.log("Response Data - " + responseObject.responseData);
	});
}