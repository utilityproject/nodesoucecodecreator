var groupCourseCategorySectionMaster = require("../models/group_course_category_section_master");
var logger = require("../../utils/logger");
var responseCode = require("../../utils/response_code");

function addGroupCourseCategorySection(requestObject, callback) {
	var newGroupCourseCategorySection = new groupCourseCategorySectionMaster({
		categoryId : requestObject.categoryId,
		sectionName : requestObject.sectionName,
		refId : requestObject.refId,
		displayOrder : requestObject.displayOrder,
		status : requestObject.status,
		createBy : requestObject.createBy
	});

	var responseObject = new Object();
	newGroupCourseCategorySection.save(function(error, data) {
		if (error) {
			logger.error(error);
			responseObject.responseCode = responseCode.MONGO_ERROR;
			callback(error, responseObject);
			return;
		}
		responseObject.responseCode = responseCode.SUCCESS;
		responseObject.responseData = data;
		callback(null, responseObject);
	});
}

module.exports = addGroupCourseCategorySection;

// Unit Test Case
if (require.main === module) {
	var requestObject = new Object();
	requestObject.categoryId = "";
	requestObject.sectionName = "";
	requestObject.refId = "";
	requestObject.displayOrder = "";
	requestObject.status = "";
	requestObject.createBy = "";
	console.log(requestObject);

	addGroupCourseCategorySection(requestObject, function(error, responseObject) {
		console.log("Response Code - " + responseObject.responseCode);
		if (error)
			console.log("Error - " + error);
		else
			console.log("Response Data - " + responseObject.responseData);
	});
}