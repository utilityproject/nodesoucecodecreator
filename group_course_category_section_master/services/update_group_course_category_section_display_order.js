var groupCourseCategorySectionMaster = require("../models/group_course_category_section_master");
var logger = require("../../utils/logger");
var responseCode = require("../../utils/response_code");
var async = require(NODE_PATH + "async");

function updateGroupCourseCategorySectionDisplayOrder(requestObject, callback) {
	var currentRowNumber = 0;
	async.each(requestObject.groupCourseCategorySectionList, function(currentRow, cb) {
		currentRowNumber++;
		console.log('currentRow - ', currentRow);

		var updateObject = new Object();
		updateObject.displayOrder = currentRowNumber;
		var responseObject = new Object();
		var query = {sectionId : currentRow.sectionId};
		groupCourseCategorySectionMaster.findOneAndUpdate(query, updateObject, function(error, data) {
			if (error) {
				logger.error(error);
				cb(error);
				return;
			}
			responseObject.responseCode = responseCode.SUCCESS;
			responseObject.responseData = data;
			cb();
		});
	}, function(error) {
		if (error) {
			responseObject.responseCode = responseCode.MONGO_ERROR;
			callback(error, responseObject);
			return;
		}
		responseObject.responseCode = responseCode.SUCCESS;
		callback(null, responseObject);
	});
}

module.exports = updateGroupCourseCategorySectionDisplayOrder;

// Unit Test Case
if (require.main === module) {
	var groupCourseCategorySectionList = [ {
		sectionId : 1,
		displayOrder : 1
	}, {
		sectionId : 2,
		displayOrder : 1
	} ];
	var responseObject = new Object();
	requestObject.groupCourseCategorySectionList = groupCourseCategorySectionList;
	console.log(requestObject);

	updateGroupCourseCategorySection(requestObject, function(error, responseObject) {
		console.log("Response Code - " + responseObject.responseCode);
		if (error)
			console.log("Error - " + error);
		else
			console.log("Response Data - " + responseObject.responseData);
	});
}