var obj = require('./controllers/index');

module.exports = function(app) {
	app.post("/addGroupCourseCategorySection", obj.addGroupCourseCategorySection);
	app.post("/updateGroupCourseCategorySection", obj.updateGroupCourseCategorySection);
	app.post("/findGroupCourseCategorySectionByProperty", obj.findGroupCourseCategorySectionByProperty);
	app.post("/findGroupCourseCategorySectionList", obj.findGroupCourseCategorySectionList);
	app.post("/updateGroupCourseCategorySectionDisplayOrder", obj.updateGroupCourseCategorySectionDisplayOrder);
// Routes
};