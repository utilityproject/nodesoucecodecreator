require("../../utils/mongo_util");
var mongoose = require("mongoose");
var autoIncrement = require("mongoose-auto-increment");

var GroupCourseCategorySectionMasterSchema = new mongoose.Schema({
	sectionId: {type: Number, unique: true},
	categoryId: {type: Number},
	sectionName: {type: String, unique: true},
	refId: {type: String, trim: true, index: true, sparse: true, unique: true},
	displayOrder: {type: Number},
	status: {type: String, enum: ["ACTIVE", "INACTIVE"]},
	createBy: Number,
	createAt: {type: Date, default: Date.now},
	updateAt: {type: Date}
});

module.exports = mongoose.model("GroupCourseCategorySectionMaster", GroupCourseCategorySectionMasterSchema, "group_course_category_section_master");

GroupCourseCategorySectionMasterSchema.plugin(autoIncrement.plugin, {
	model: "GroupCourseCategorySectionMaster",
	field: "sectionId",
	startAt: 1,
	incrementBy: 1
});