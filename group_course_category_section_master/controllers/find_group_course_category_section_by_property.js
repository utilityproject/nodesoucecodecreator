var logger = require("../../utils/logger");
var groupCourseCategorySectionService = require("../services/index");
var responseMessage = require("../../utils/response_message");
var responseCode = require("../../utils/response_code");

function findGroupCourseCategorySectionByProperty(request, response, next) {
	var requestObject = request.body;
	//console.log("findGroupCourseCategorySectionByProperty API :- Request - %j", requestObject);

	var responseObject = new Object();
	groupCourseCategorySectionService.findGroupCourseCategorySectionByProperty(requestObject, function(error, data) {
		if(error === null) {
			responseObject.responseCode = data.responseCode;
			responseObject.responseData = data.responseData;
		} else if(data.responseCode !== responseCode.SUCCESS) {
			responseObject.responseCode = data.responseCode;
			responseObject.responseData = {};
			responseObject.responseData.message = responseMessage[data.responseCode];
		} else {
			responseObject.responseCode = data.responseCode;

			var currentRow = data.responseData;
			var currentObj = {};
			currentObj.categoryId = currentRow.categoryId;
			currentObj.sectionName = currentRow.sectionName;
			currentObj.refId = currentRow.refId;
			currentObj.displayOrder = currentRow.displayOrder;
			currentObj.status = currentRow.status;

			responseObject.responseData = currentObj;
		}

		logger.info("findGroupCourseCategorySectionByProperty API :- Response - %j", responseObject);
		response.json(responseObject);
	});
}

module.exports = findGroupCourseCategorySectionByProperty;

// Unit Test Case
if (require.main === module) {
	(function() {
		var request = {};
		var response = {
			json : function(result) {
				console.log(JSON.stringify(result, null, 2));
			}
		};

		var requestObject = new Object();
		//requestObject.categoryId = "categoryId";
		//requestObject.sectionName = "sectionName";
		//requestObject.refId = "refId";
		//requestObject.displayOrder = "displayOrder";
		//requestObject.status = "status";

		console.log("Request Data - " + requestObject);
		request.body = requestObject;
		findGroupCourseCategorySectionByProperty(request, response);
	})();
}