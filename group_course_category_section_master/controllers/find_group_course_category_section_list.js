var logger = require("../../utils/logger");
var groupCourseCategorySectionService = require("../services/index");
var responseMessage = require("../../utils/response_message");
var responseCode = require("../../utils/response_code");

function findGroupCourseCategorySectionList(request, response, next) {
	var requestObject = request.body;
	//console.log("findGroupCourseCategorySectionList API :- Request - %j", requestObject);

	var responseObject = new Object();
	groupCourseCategorySectionService.findGroupCourseCategorySectionList(requestObject, function(error, data) {
		if(error === null) {
			responseObject.responseCode = data.responseCode;
			responseObject.responseData = data.responseData;
		} else if(data.responseCode !== responseCode.SUCCESS) {
			responseObject.responseCode = data.responseCode;
			responseObject.responseData = {};
			responseObject.responseData.message = responseMessage[data.responseCode];
		} else {
			responseObject.responseCode = data.responseCode;

			var groupCourseCategorySectionArr = data.responseData;
			var groupCourseCategorySectionList = [];
			for (var i=0, length=groupCourseCategorySectionArr.length; i<length; i++) {
				var currentRow = groupCourseCategorySectionArr[i];
				var currentObj = {};
				currentObj.categoryId = currentRow.categoryId;
				currentObj.sectionName = currentRow.sectionName;
				currentObj.refId = currentRow.refId;
				currentObj.displayOrder = currentRow.displayOrder;
				currentObj.status = currentRow.status;
				groupCourseCategorySectionList.push(currentObj);
			}
			responseObject.responseData = groupCourseCategorySectionList;
		}

		logger.info("findGroupCourseCategorySectionList API :- Response - %j", responseObject);
		response.json(responseObject);
	});
}

module.exports = findGroupCourseCategorySectionList;

// Unit Test Case
if (require.main === module) {
	(function() {
		var request = {};
		var response = {
			json : function(result) {
				console.log(JSON.stringify(result, null, 2));
			}
		};

		var requestObject = new Object();
		//requestObject.status = "ACTIVE";

		console.log("Request Data - " + requestObject);
		request.body = requestObject;
		findGroupCourseCategorySectionList(request, response);
	})();
}