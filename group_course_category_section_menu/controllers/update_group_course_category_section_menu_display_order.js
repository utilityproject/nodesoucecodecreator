var logger = require("../../utils/logger");
var menuService = require("../services/index");
var responseMessage = require("../../utils/response_message");
var responseCode = require("../../utils/response_code");

function updateMenuDisplayOrder(request, response, next) {
	var requestObject = request.body;
	//console.log("updateMenuDisplayOrder API :- Request - %j", requestObject);

	var responseObject = new Object();
	menuService.updateMenuDisplayOrder(requestObject, function(error, data) {
		if(error === null) {
			responseObject.responseCode = data.responseCode;
			responseObject.responseData = data.responseData;
		} else if(data.responseCode !== responseCode.SUCCESS) {
			responseObject.responseCode = data.responseCode;
			responseObject.responseData = {};
			responseObject.responseData.message = responseMessage[data.responseCode];
		} else {
			responseObject.responseCode = data.responseCode;
			responseObject.responseData = data.responseData;
		}

		logger.info("updateMenuDisplayOrder API :- Response - %j", responseObject);
		response.json(responseObject);
	});
}

module.exports = updateMenuDisplayOrder;