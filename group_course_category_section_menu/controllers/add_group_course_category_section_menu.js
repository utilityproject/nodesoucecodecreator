var logger = require("../../utils/logger");
var menuService = require("../services/index");
var responseMessage = require("../../utils/response_message");
var responseCode = require("../../utils/response_code");

function addMenu(request, response, next) {
	var requestObject = request.body;
	//console.log("addMenu API :- Request - %j", requestObject);

	var responseObject = new Object();
	menuService.addMenu(requestObject, function(error, data) {
		if(error === null) {
			responseObject.responseCode = data.responseCode;
			responseObject.responseData = data.responseData;
		} else if(data.responseCode !== responseCode.SUCCESS) {
			responseObject.responseCode = data.responseCode;
			responseObject.responseData = {};
			responseObject.responseData.message = responseMessage[data.responseCode];
		} else {
			responseObject.responseCode = data.responseCode;
			responseObject.responseData = data.responseData;
		}

		logger.info("addMenu API :- Response - %j", responseObject);
		response.json(responseObject);
	});
}

module.exports = addMenu;

// Unit Test Case
if (require.main === module) {
	(function() {
		var request = {};
		var response = {
			json : function(result) {
				console.log(JSON.stringify(result, null, 2));
			}
		};

		var requestObject = new Object();
		requestObject.sectionId = "";
		requestObject.categoryId = "";
		requestObject.menuName = "";
		requestObject.urlName = "";
		requestObject.metaTitle = "";
		requestObject.metaKeywords = "";
		requestObject.metaDescription = "";
		requestObject.displayOrder = "";
		requestObject.status = "";
		requestObject.createBy = "";

		console.log("Request Data - " + requestObject);
		request.body = requestObject;
		addMenu(request, response);
	})();
}