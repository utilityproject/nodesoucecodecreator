var logger = require("../../utils/logger");
var menuService = require("../services/index");
var responseMessage = require("../../utils/response_message");
var responseCode = require("../../utils/response_code");

function findMenuByProperty(request, response, next) {
	var requestObject = request.body;
	//console.log("findMenuByProperty API :- Request - %j", requestObject);

	var responseObject = new Object();
	menuService.findMenuByProperty(requestObject, function(error, data) {
		if(error === null) {
			responseObject.responseCode = data.responseCode;
			responseObject.responseData = data.responseData;
		} else if(data.responseCode !== responseCode.SUCCESS) {
			responseObject.responseCode = data.responseCode;
			responseObject.responseData = {};
			responseObject.responseData.message = responseMessage[data.responseCode];
		} else {
			responseObject.responseCode = data.responseCode;

			var currentRow = data.responseData;
			var currentObj = {};
			currentObj.sectionId = currentRow.sectionId;
			currentObj.categoryId = currentRow.categoryId;
			currentObj.menuName = currentRow.menuName;
			currentObj.urlName = currentRow.seoData.urlName;
			currentObj.metaTitle = currentRow.seoData.metaTitle;
			currentObj.metaKeywords = currentRow.seoData.metaKeywords;
			currentObj.metaDescription = currentRow.seoData.metaDescription;
			currentObj.displayOrder = currentRow.displayOrder;
			currentObj.status = currentRow.status;

			responseObject.responseData = currentObj;
		}

		logger.info("findMenuByProperty API :- Response - %j", responseObject);
		response.json(responseObject);
	});
}

module.exports = findMenuByProperty;

// Unit Test Case
if (require.main === module) {
	(function() {
		var request = {};
		var response = {
			json : function(result) {
				console.log(JSON.stringify(result, null, 2));
			}
		};

		var requestObject = new Object();
		//requestObject.sectionId = "sectionId";
		//requestObject.categoryId = "categoryId";
		//requestObject.menuName = "menuName";
		//requestObject.seoData = "seoData";
		//requestObject.displayOrder = "displayOrder";
		//requestObject.status = "status";

		console.log("Request Data - " + requestObject);
		request.body = requestObject;
		findMenuByProperty(request, response);
	})();
}