var logger = require("../../utils/logger");
var menuService = require("../services/index");
var responseMessage = require("../../utils/response_message");
var responseCode = require("../../utils/response_code");

function findMenuList(request, response, next) {
	var requestObject = request.body;
	//console.log("findMenuList API :- Request - %j", requestObject);

	var responseObject = new Object();
	menuService.findMenuList(requestObject, function(error, data) {
		if(error === null) {
			responseObject.responseCode = data.responseCode;
			responseObject.responseData = data.responseData;
		} else if(data.responseCode !== responseCode.SUCCESS) {
			responseObject.responseCode = data.responseCode;
			responseObject.responseData = {};
			responseObject.responseData.message = responseMessage[data.responseCode];
		} else {
			responseObject.responseCode = data.responseCode;

			var menuArr = data.responseData;
			var menuList = [];
			for (var i=0, length=menuArr.length; i<length; i++) {
				var currentRow = menuArr[i];
				var currentObj = {};
				currentObj.sectionId = currentRow.sectionId;
				currentObj.categoryId = currentRow.categoryId;
				currentObj.menuName = currentRow.menuName;
				currentObj.urlName = currentRow.seoData.urlName;
				currentObj.metaTitle = currentRow.seoData.metaTitle;
				currentObj.metaKeywords = currentRow.seoData.metaKeywords;
				currentObj.metaDescription = currentRow.seoData.metaDescription;
				currentObj.displayOrder = currentRow.displayOrder;
				currentObj.status = currentRow.status;
				menuList.push(currentObj);
			}
			responseObject.responseData = menuList;
		}

		logger.info("findMenuList API :- Response - %j", responseObject);
		response.json(responseObject);
	});
}

module.exports = findMenuList;

// Unit Test Case
if (require.main === module) {
	(function() {
		var request = {};
		var response = {
			json : function(result) {
				console.log(JSON.stringify(result, null, 2));
			}
		};

		var requestObject = new Object();
		//requestObject.status = "ACTIVE";

		console.log("Request Data - " + requestObject);
		request.body = requestObject;
		findMenuList(request, response);
	})();
}