var menuMaster = require("../models/group_course_category_section_menu_master");
var logger = require("../../utils/logger");
var responseCode = require("../../utils/response_code");
var async = require(NODE_PATH + "async");

function updateMenuDisplayOrder(requestObject, callback) {
	var currentRowNumber = 0;
	async.each(requestObject.menuList, function(currentRow, cb) {
		currentRowNumber++;
		console.log('currentRow - ', currentRow);

		var updateObject = new Object();
		updateObject.displayOrder = currentRowNumber;
		var responseObject = new Object();
		var query = {menuId : currentRow.menuId};
		menuMaster.findOneAndUpdate(query, updateObject, function(error, data) {
			if (error) {
				logger.error(error);
				cb(error);
				return;
			}
			responseObject.responseCode = responseCode.SUCCESS;
			responseObject.responseData = data;
			cb();
		});
	}, function(error) {
		if (error) {
			responseObject.responseCode = responseCode.MONGO_ERROR;
			callback(error, responseObject);
			return;
		}
		responseObject.responseCode = responseCode.SUCCESS;
		callback(null, responseObject);
	});
}

module.exports = updateMenuDisplayOrder;

// Unit Test Case
if (require.main === module) {
	var menuList = [ {
		menuId : 1,
		displayOrder : 1
	}, {
		menuId : 2,
		displayOrder : 1
	} ];
	var responseObject = new Object();
	requestObject.menuList = menuList;
	console.log(requestObject);

	updateMenu(requestObject, function(error, responseObject) {
		console.log("Response Code - " + responseObject.responseCode);
		if (error)
			console.log("Error - " + error);
		else
			console.log("Response Data - " + responseObject.responseData);
	});
}