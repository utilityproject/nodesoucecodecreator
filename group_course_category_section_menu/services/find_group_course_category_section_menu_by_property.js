var menuMaster = require("../models/group_course_category_section_menu_master");
var logger = require("../../utils/logger");
var responseCode = require("../../utils/response_code");

function findMenuByProperty(requestObject, callback) {
	var query = menuMaster.findOne({});
	if (typeof requestObject.menuId !== "undefined" && requestObject.menuId !== null)
		query.where("menuId").equals(requestObject.menuId);
	if (typeof requestObject.sectionId !== "undefined" && requestObject.sectionId !== null)
		query.where("sectionId").equals(requestObject.sectionId);
	if (typeof requestObject.categoryId !== "undefined" && requestObject.categoryId !== null)
		query.where("categoryId").equals(requestObject.categoryId);
	if (typeof requestObject.menuName !== "undefined" && requestObject.menuName !== null)
		query.where("menuName").equals(requestObject.menuName);

	var responseObject = new Object();
	query.exec(function (error, data) {
		if (error) {
			logger.error(error);
			responseObject.responseCode = responseCode.MONGO_ERROR;
			callback(error, responseObject);
			return;
		}
		responseObject.responseCode = responseCode.SUCCESS;
		responseObject.responseData = data;
		callback(null, responseObject);
	});
}

module.exports = findMenuByProperty;

// Unit Test Case
if (require.main === module) {
	var requestObject = new Object();
	//requestObject.menuId = "menuId";
	//requestObject.sectionId = "sectionId";
	//requestObject.categoryId = "categoryId";
	//requestObject.menuName = "menuName";

	findMenuByProperty(requestObject, function(error, responseObject) {
		console.log("Response Code - " + responseObject.responseCode);
		if (error)
			console.log("Error - " + error);
		else
			console.log("Response Data - " + responseObject.responseData);
	});
}