var menuMaster = require("../models/group_course_category_section_menu_master");
var logger = require("../../utils/logger");
var responseCode = require("../../utils/response_code");

function updateMenu(requestObject, callback) {
	var updateObject = new Object();
	if(requestObject.sectionId)
		updateObject.sectionId = requestObject.sectionId;
	if(requestObject.categoryId)
		updateObject.categoryId = requestObject.categoryId;
	if(requestObject.menuName)
		updateObject.menuName = requestObject.menuName;
	if(requestObject.urlName)
		updateObject["seoData.urlName"] = requestObject.urlName;
	if(requestObject.metaTitle)
		updateObject["seoData.metaTitle"] = requestObject.metaTitle;
	if(requestObject.metaKeywords)
		updateObject["seoData.metaKeywords"] = requestObject.metaKeywords;
	if(requestObject.metaDescription)
		updateObject["seoData.metaDescription"] = requestObject.metaDescription;
	if(requestObject.displayOrder)
		updateObject.displayOrder = requestObject.displayOrder;
	if(requestObject.status)
		updateObject.status = requestObject.status;
	updateObject.updateAt = new Date();

	var responseObject = new Object();
	var query = {menuId: requestObject.menuId};
	menuMaster.findOneAndUpdate(query, updateObject, function (error, data) {
		if (error) {
			logger.error(error);
			responseObject.responseCode = responseCode.MONGO_ERROR;
			callback(error, responseObject);
			return;
		}
		responseObject.responseCode = responseCode.SUCCESS;
		responseObject.responseData = data;
		callback(null, responseObject);
	});
}

module.exports = updateMenu;

// Unit Test Case
if (require.main === module) {
	var requestObject = new Object();
	requestObject.sectionId = "";
	requestObject.categoryId = "";
	requestObject.menuName = "";
	requestObject.urlName = "";
	requestObject.metaTitle = "";
	requestObject.metaKeywords = "";
	requestObject.metaDescription = "";
	requestObject.displayOrder = "";
	requestObject.status = "";
	console.log(requestObject);

	updateMenu(requestObject, function(error, responseObject) {
		console.log("Response Code - " + responseObject.responseCode);
		if (error)
			console.log("Error - " + error);
		else
			console.log("Response Data - " + responseObject.responseData);
	});
}