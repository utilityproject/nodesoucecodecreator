var menuMaster = require("../models/group_course_category_section_menu_master");
var logger = require("../../utils/logger");
var responseCode = require("../../utils/response_code");

function addMenu(requestObject, callback) {
	var newMenu = new menuMaster({
		sectionId : requestObject.sectionId,
		categoryId : requestObject.categoryId,
		menuName : requestObject.menuName,
		seoData : {
			urlName : requestObject.urlName,
			metaTitle : requestObject.metaTitle,
			metaKeywords : requestObject.metaKeywords,
			metaDescription : requestObject.metaDescription
		},
		displayOrder : requestObject.displayOrder,
		status : requestObject.status,
		createBy : requestObject.createBy
	});

	var responseObject = new Object();
	newMenu.save(function(error, data) {
		if (error) {
			logger.error(error);
			responseObject.responseCode = responseCode.MONGO_ERROR;
			callback(error, responseObject);
			return;
		}
		responseObject.responseCode = responseCode.SUCCESS;
		responseObject.responseData = data;
		callback(null, responseObject);
	});
}

module.exports = addMenu;

// Unit Test Case
if (require.main === module) {
	var requestObject = new Object();
	requestObject.sectionId = "";
	requestObject.categoryId = "";
	requestObject.menuName = "";
	requestObject.urlName = "";
	requestObject.metaTitle = "";
	requestObject.metaKeywords = "";
	requestObject.metaDescription = "";
	requestObject.displayOrder = "";
	requestObject.status = "";
	requestObject.createBy = "";
	console.log(requestObject);

	addMenu(requestObject, function(error, responseObject) {
		console.log("Response Code - " + responseObject.responseCode);
		if (error)
			console.log("Error - " + error);
		else
			console.log("Response Data - " + responseObject.responseData);
	});
}