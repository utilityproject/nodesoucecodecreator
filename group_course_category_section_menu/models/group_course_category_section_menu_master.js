require("../../utils/mongo_util");
var mongoose = require("mongoose");
var autoIncrement = require("mongoose-auto-increment");

var MenuMasterSchema = new mongoose.Schema({
	menuId: {type: Number, unique: true},
	sectionId: {type: Number},
	categoryId: {type: Number},
	menuName: {type: String, unique: true},
	seoData: {
		urlName: String,
		metaTitle: String,
		metaKeywords: String,
		metaDescription: String
	},
	displayOrder: {type: Number},
	status: {type: String, enum: ["ACTIVE", "INACTIVE"]},
	createBy: Number,
	createAt: {type: Date, default: Date.now},
	updateAt: {type: Date}
});

module.exports = mongoose.model("MenuMaster", MenuMasterSchema, "group_course_category_section_menu_master");

MenuMasterSchema.plugin(autoIncrement.plugin, {
	model: "MenuMaster",
	field: "menuId",
	startAt: 1,
	incrementBy: 1
});