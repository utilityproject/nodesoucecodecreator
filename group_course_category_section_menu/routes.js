var obj = require('./controllers/index');

module.exports = function(app) {
	app.post("/addMenu", obj.addMenu);
	app.post("/updateMenu", obj.updateMenu);
	app.post("/findMenuByProperty", obj.findMenuByProperty);
	app.post("/findMenuList", obj.findMenuList);
	app.post("/updateMenuDisplayOrder", obj.updateMenuDisplayOrder);
// Routes
};