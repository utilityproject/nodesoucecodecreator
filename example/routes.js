var obj = require('./controllers/index');

module.exports = function(app) {
	app.post("/addExample", obj.addExample);
	app.post("/updateExample", obj.updateExample);
	app.post("/findExampleByProperty", obj.findExampleByProperty);
	app.post("/findExampleList", obj.findExampleList);
	app.post("/updateExampleDisplayOrder", obj.updateExampleDisplayOrder);
// Routes
};