var exampleMaster = require("../models/example_master");
var logger = require("../../utils/logger");
var responseCode = require("../../utils/response_code");

function updateExample(requestObject, callback) {
	var updateObject = new Object();
	if(requestObject.groupId)
		updateObject.groupId = requestObject.groupId;
	if(requestObject.courseId)
		updateObject.courseId = requestObject.courseId;
	if(requestObject.sectionId)
		updateObject.sectionId = requestObject.sectionId;
	if(requestObject.categoryId)
		updateObject.categoryId = requestObject.categoryId;
	if(requestObject.menuId)
		updateObject.menuId = requestObject.menuId;
	if(requestObject.exampleTitle)
		updateObject.exampleTitle = requestObject.exampleTitle;
	if(requestObject.example)
		updateObject.example = requestObject.example;
	if(requestObject.exampleDescription)
		updateObject.exampleDescription = requestObject.exampleDescription;
	if(requestObject.views)
		updateObject.views = requestObject.views;
	if(requestObject.likes)
		updateObject.likes = requestObject.likes;
	if(requestObject.comments)
		updateObject.comments = requestObject.comments;
	if(requestObject.displayOrder)
		updateObject.displayOrder = requestObject.displayOrder;
	if(requestObject.status)
		updateObject.status = requestObject.status;
	updateObject.updateAt = new Date();

	var responseObject = new Object();
	var query = {exampleId: requestObject.exampleId};
	exampleMaster.findOneAndUpdate(query, updateObject, function (error, data) {
		if (error) {
			logger.error(error);
			responseObject.responseCode = responseCode.MONGO_ERROR;
			callback(error, responseObject);
			return;
		}
		responseObject.responseCode = responseCode.SUCCESS;
		responseObject.responseData = data;
		callback(null, responseObject);
	});
}

module.exports = updateExample;

// Unit Test Case
if (require.main === module) {
	var requestObject = new Object();
	requestObject.groupId = "";
	requestObject.courseId = "";
	requestObject.sectionId = "";
	requestObject.categoryId = "";
	requestObject.menuId = "";
	requestObject.exampleTitle = "";
	requestObject.example = "";
	requestObject.exampleDescription = "";
	requestObject.views = "";
	requestObject.likes = "";
	requestObject.comments = "";
	requestObject.displayOrder = "";
	requestObject.status = "";
	console.log(requestObject);

	updateExample(requestObject, function(error, responseObject) {
		console.log("Response Code - " + responseObject.responseCode);
		if (error)
			console.log("Error - " + error);
		else
			console.log("Response Data - " + responseObject.responseData);
	});
}