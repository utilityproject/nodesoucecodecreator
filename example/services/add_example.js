var exampleMaster = require("../models/example_master");
var logger = require("../../utils/logger");
var responseCode = require("../../utils/response_code");

function addExample(requestObject, callback) {
	var newExample = new exampleMaster({
		groupId : requestObject.groupId,
		courseId : requestObject.courseId,
		sectionId : requestObject.sectionId,
		categoryId : requestObject.categoryId,
		menuId : requestObject.menuId,
		exampleTitle : requestObject.exampleTitle,
		example : requestObject.example,
		exampleDescription : requestObject.exampleDescription,
		views : requestObject.views,
		likes : requestObject.likes,
		comments : requestObject.comments,
		displayOrder : requestObject.displayOrder,
		status : requestObject.status,
		createBy : requestObject.createBy
	});

	var responseObject = new Object();
	newExample.save(function(error, data) {
		if (error) {
			logger.error(error);
			responseObject.responseCode = responseCode.MONGO_ERROR;
			callback(error, responseObject);
			return;
		}
		responseObject.responseCode = responseCode.SUCCESS;
		responseObject.responseData = data;
		callback(null, responseObject);
	});
}

module.exports = addExample;

// Unit Test Case
if (require.main === module) {
	var requestObject = new Object();
	requestObject.groupId = "";
	requestObject.courseId = "";
	requestObject.sectionId = "";
	requestObject.categoryId = "";
	requestObject.menuId = "";
	requestObject.exampleTitle = "";
	requestObject.example = "";
	requestObject.exampleDescription = "";
	requestObject.views = "";
	requestObject.likes = "";
	requestObject.comments = "";
	requestObject.displayOrder = "";
	requestObject.status = "";
	requestObject.createBy = "";
	console.log(requestObject);

	addExample(requestObject, function(error, responseObject) {
		console.log("Response Code - " + responseObject.responseCode);
		if (error)
			console.log("Error - " + error);
		else
			console.log("Response Data - " + responseObject.responseData);
	});
}