var exampleMaster = require("../models/example_master");
var logger = require("../../utils/logger");
var responseCode = require("../../utils/response_code");

function findExampleByProperty(requestObject, callback) {
	var query = exampleMaster.findOne({});
	if (typeof requestObject.exampleId !== "undefined" && requestObject.exampleId !== null)
		query.where("exampleId").equals(requestObject.exampleId);
	if (typeof requestObject.groupId !== "undefined" && requestObject.groupId !== null)
		query.where("groupId").equals(requestObject.groupId);
	if (typeof requestObject.courseId !== "undefined" && requestObject.courseId !== null)
		query.where("courseId").equals(requestObject.courseId);
	if (typeof requestObject.sectionId !== "undefined" && requestObject.sectionId !== null)
		query.where("sectionId").equals(requestObject.sectionId);
	if (typeof requestObject.categoryId !== "undefined" && requestObject.categoryId !== null)
		query.where("categoryId").equals(requestObject.categoryId);
	if (typeof requestObject.menuId !== "undefined" && requestObject.menuId !== null)
		query.where("menuId").equals(requestObject.menuId);
	if (typeof requestObject.exampleTitle !== "undefined" && requestObject.exampleTitle !== null)
		query.where("exampleTitle").equals(requestObject.exampleTitle);
	if (typeof requestObject.example !== "undefined" && requestObject.example !== null)
		query.where("example").equals(requestObject.example);
	if (typeof requestObject.exampleDescription !== "undefined" && requestObject.exampleDescription !== null)
		query.where("exampleDescription").equals(requestObject.exampleDescription);
	if (typeof requestObject.views !== "undefined" && requestObject.views !== null)
		query.where("views").equals(requestObject.views);
	if (typeof requestObject.likes !== "undefined" && requestObject.likes !== null)
		query.where("likes").equals(requestObject.likes);
	if (typeof requestObject.comments !== "undefined" && requestObject.comments !== null)
		query.where("comments").equals(requestObject.comments);

	var responseObject = new Object();
	query.exec(function (error, data) {
		if (error) {
			logger.error(error);
			responseObject.responseCode = responseCode.MONGO_ERROR;
			callback(error, responseObject);
			return;
		}
		responseObject.responseCode = responseCode.SUCCESS;
		responseObject.responseData = data;
		callback(null, responseObject);
	});
}

module.exports = findExampleByProperty;

// Unit Test Case
if (require.main === module) {
	var requestObject = new Object();
	//requestObject.exampleId = "exampleId";
	//requestObject.groupId = "groupId";
	//requestObject.courseId = "courseId";
	//requestObject.sectionId = "sectionId";
	//requestObject.categoryId = "categoryId";
	//requestObject.menuId = "menuId";
	//requestObject.exampleTitle = "exampleTitle";
	//requestObject.example = "example";
	//requestObject.exampleDescription = "exampleDescription";
	//requestObject.views = "views";
	//requestObject.likes = "likes";
	//requestObject.comments = "comments";

	findExampleByProperty(requestObject, function(error, responseObject) {
		console.log("Response Code - " + responseObject.responseCode);
		if (error)
			console.log("Error - " + error);
		else
			console.log("Response Data - " + responseObject.responseData);
	});
}