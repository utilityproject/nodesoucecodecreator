require("../../utils/mongo_util");
var mongoose = require("mongoose");
var autoIncrement = require("mongoose-auto-increment");

var ExampleMasterSchema = new mongoose.Schema({
	exampleId: {type: Number, unique: true},
	groupId: {type: Number},
	courseId: {type: Number},
	sectionId: {type: Number},
	categoryId: {type: Number},
	menuId: {type: Number},
	exampleTitle: {type: String},
	example: {type: String},
	exampleDescription: {type: String},
	views: {type: String},
	likes: {type: String},
	comments: {type: String},
	displayOrder: {type: Number},
	status: {type: String, enum: ["ACTIVE", "INACTIVE"]},
	createBy: Number,
	createAt: {type: Date, default: Date.now},
	updateAt: {type: Date}
});

module.exports = mongoose.model("ExampleMaster", ExampleMasterSchema, "exmple_master");

ExampleMasterSchema.plugin(autoIncrement.plugin, {
	model: "ExampleMaster",
	field: "exampleId",
	startAt: 1,
	incrementBy: 1
});