var logger = require("../../utils/logger");
var exampleService = require("../services/index");
var responseMessage = require("../../utils/response_message");
var responseCode = require("../../utils/response_code");

function findExampleList(request, response, next) {
	var requestObject = request.body;
	//console.log("findExampleList API :- Request - %j", requestObject);

	var responseObject = new Object();
	exampleService.findExampleList(requestObject, function(error, data) {
		if(error === null) {
			responseObject.responseCode = data.responseCode;
			responseObject.responseData = data.responseData;
		} else if(data.responseCode !== responseCode.SUCCESS) {
			responseObject.responseCode = data.responseCode;
			responseObject.responseData = {};
			responseObject.responseData.message = responseMessage[data.responseCode];
		} else {
			responseObject.responseCode = data.responseCode;

			var exampleArr = data.responseData;
			var exampleList = [];
			for (var i=0, length=exampleArr.length; i<length; i++) {
				var currentRow = exampleArr[i];
				var currentObj = {};
				currentObj.groupId = currentRow.groupId;
				currentObj.courseId = currentRow.courseId;
				currentObj.sectionId = currentRow.sectionId;
				currentObj.categoryId = currentRow.categoryId;
				currentObj.menuId = currentRow.menuId;
				currentObj.exampleTitle = currentRow.exampleTitle;
				currentObj.example = currentRow.example;
				currentObj.exampleDescription = currentRow.exampleDescription;
				currentObj.views = currentRow.views;
				currentObj.likes = currentRow.likes;
				currentObj.comments = currentRow.comments;
				currentObj.displayOrder = currentRow.displayOrder;
				currentObj.status = currentRow.status;
				exampleList.push(currentObj);
			}
			responseObject.responseData = exampleList;
		}

		logger.info("findExampleList API :- Response - %j", responseObject);
		response.json(responseObject);
	});
}

module.exports = findExampleList;

// Unit Test Case
if (require.main === module) {
	(function() {
		var request = {};
		var response = {
			json : function(result) {
				console.log(JSON.stringify(result, null, 2));
			}
		};

		var requestObject = new Object();
		//requestObject.status = "ACTIVE";

		console.log("Request Data - " + requestObject);
		request.body = requestObject;
		findExampleList(request, response);
	})();
}