var logger = require("../../utils/logger");
var exampleService = require("../services/index");
var responseMessage = require("../../utils/response_message");
var responseCode = require("../../utils/response_code");

function updateExampleDisplayOrder(request, response, next) {
	var requestObject = request.body;
	//console.log("updateExampleDisplayOrder API :- Request - %j", requestObject);

	var responseObject = new Object();
	exampleService.updateExampleDisplayOrder(requestObject, function(error, data) {
		if(error === null) {
			responseObject.responseCode = data.responseCode;
			responseObject.responseData = data.responseData;
		} else if(data.responseCode !== responseCode.SUCCESS) {
			responseObject.responseCode = data.responseCode;
			responseObject.responseData = {};
			responseObject.responseData.message = responseMessage[data.responseCode];
		} else {
			responseObject.responseCode = data.responseCode;
			responseObject.responseData = data.responseData;
		}

		logger.info("updateExampleDisplayOrder API :- Response - %j", responseObject);
		response.json(responseObject);
	});
}

module.exports = updateExampleDisplayOrder;