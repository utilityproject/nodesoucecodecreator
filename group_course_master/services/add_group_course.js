var groupCourseMaster = require("../models/group_course_master");
var logger = require("../../utils/logger");
var responseCode = require("../../utils/response_code");

function addGroupCourse(requestObject, callback) {
	var newGroupCourse = new groupCourseMaster({
		groupId : requestObject.groupId,
		courseName : requestObject.courseName,
		courseDevName : requestObject.courseDevName,
		defaultCourseLink : requestObject.defaultCourseLink,
		refId : requestObject.refId,
		displayOrder : requestObject.displayOrder,
		status : requestObject.status,
		createBy : requestObject.createBy
	});

	var responseObject = new Object();
	newGroupCourse.save(function(error, data) {
		if (error) {
			logger.error(error);
			responseObject.responseCode = responseCode.MONGO_ERROR;
			callback(error, responseObject);
			return;
		}
		responseObject.responseCode = responseCode.SUCCESS;
		responseObject.responseData = data;
		callback(null, responseObject);
	});
}

module.exports = addGroupCourse;

// Unit Test Case
if (require.main === module) {
	var requestObject = new Object();
	requestObject.groupId = "";
	requestObject.courseName = "";
	requestObject.courseDevName = "";
	requestObject.defaultCourseLink = "";
	requestObject.refId = "";
	requestObject.displayOrder = "";
	requestObject.status = "";
	requestObject.createBy = "";
	console.log(requestObject);

	addGroupCourse(requestObject, function(error, responseObject) {
		console.log("Response Code - " + responseObject.responseCode);
		if (error)
			console.log("Error - " + error);
		else
			console.log("Response Data - " + responseObject.responseData);
	});
}