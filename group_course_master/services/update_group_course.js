var groupCourseMaster = require("../models/group_course_master");
var logger = require("../../utils/logger");
var responseCode = require("../../utils/response_code");

function updateGroupCourse(requestObject, callback) {
	var updateObject = new Object();
	if(requestObject.groupId)
		updateObject.groupId = requestObject.groupId;
	if(requestObject.courseName)
		updateObject.courseName = requestObject.courseName;
	if(requestObject.courseDevName)
		updateObject.courseDevName = requestObject.courseDevName;
	if(requestObject.defaultCourseLink)
		updateObject.defaultCourseLink = requestObject.defaultCourseLink;
	if(requestObject.refId)
		updateObject.refId = requestObject.refId;
	if(requestObject.displayOrder)
		updateObject.displayOrder = requestObject.displayOrder;
	if(requestObject.status)
		updateObject.status = requestObject.status;
	updateObject.updateAt = new Date();

	var responseObject = new Object();
	var query = {courseId: requestObject.courseId};
	groupCourseMaster.findOneAndUpdate(query, updateObject, function (error, data) {
		if (error) {
			logger.error(error);
			responseObject.responseCode = responseCode.MONGO_ERROR;
			callback(error, responseObject);
			return;
		}
		responseObject.responseCode = responseCode.SUCCESS;
		responseObject.responseData = data;
		callback(null, responseObject);
	});
}

module.exports = updateGroupCourse;

// Unit Test Case
if (require.main === module) {
	var requestObject = new Object();
	requestObject.groupId = "";
	requestObject.courseName = "";
	requestObject.courseDevName = "";
	requestObject.defaultCourseLink = "";
	requestObject.refId = "";
	requestObject.displayOrder = "";
	requestObject.status = "";
	console.log(requestObject);

	updateGroupCourse(requestObject, function(error, responseObject) {
		console.log("Response Code - " + responseObject.responseCode);
		if (error)
			console.log("Error - " + error);
		else
			console.log("Response Data - " + responseObject.responseData);
	});
}