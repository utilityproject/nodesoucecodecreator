require("../../utils/mongo_util");
var mongoose = require("mongoose");
var autoIncrement = require("mongoose-auto-increment");

var GroupCourseMasterSchema = new mongoose.Schema({
	courseId: {type: Number, unique: true},
	groupId: {type: Number},
	courseName: {type: String, unique: true},
	imageLink: {type: String},
	courseDevName: {type: String, unique: true},
	defaultCourseLink: {type: String},
	refId: {type: String, trim: true, index: true, sparse: true, unique: true},
	displayOrder: {type: Number},
	status: {type: String, enum: ["ACTIVE", "INACTIVE"]},
	createBy: Number,
	createAt: {type: Date, default: Date.now},
	updateAt: {type: Date}
});

module.exports = mongoose.model("GroupCourseMaster", GroupCourseMasterSchema, "group_course_master");

GroupCourseMasterSchema.plugin(autoIncrement.plugin, {
	model: "GroupCourseMaster",
	field: "courseId",
	startAt: 1,
	incrementBy: 1
});