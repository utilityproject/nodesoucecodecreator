var logger = require("../../utils/logger");
var groupCourseService = require("../services/index");
var responseMessage = require("../../utils/response_message");
var responseCode = require("../../utils/response_code");

function findGroupCourseByProperty(request, response, next) {
	var requestObject = request.body;
	//console.log("findGroupCourseByProperty API :- Request - %j", requestObject);

	var responseObject = new Object();
	groupCourseService.findGroupCourseByProperty(requestObject, function(error, data) {
		if(error === null) {
			responseObject.responseCode = data.responseCode;
			responseObject.responseData = data.responseData;
		} else if(data.responseCode !== responseCode.SUCCESS) {
			responseObject.responseCode = data.responseCode;
			responseObject.responseData = {};
			responseObject.responseData.message = responseMessage[data.responseCode];
		} else {
			responseObject.responseCode = data.responseCode;

			var currentRow = data.responseData;
			var currentObj = {};
			currentObj.groupId = currentRow.groupId;
			currentObj.courseName = currentRow.courseName;
			currentObj.courseDevName = currentRow.courseDevName;
			currentObj.defaultCourseLink = currentRow.defaultCourseLink;
			currentObj.refId = currentRow.refId;
			currentObj.displayOrder = currentRow.displayOrder;
			currentObj.status = currentRow.status;

			responseObject.responseData = currentObj;
		}

		logger.info("findGroupCourseByProperty API :- Response - %j", responseObject);
		response.json(responseObject);
	});
}

module.exports = findGroupCourseByProperty;

// Unit Test Case
if (require.main === module) {
	(function() {
		var request = {};
		var response = {
			json : function(result) {
				console.log(JSON.stringify(result, null, 2));
			}
		};

		var requestObject = new Object();
		//requestObject.groupId = "groupId";
		//requestObject.courseName = "courseName";
		//requestObject.courseDevName = "courseDevName";
		//requestObject.defaultCourseLink = "defaultCourseLink";
		//requestObject.refId = "refId";
		//requestObject.displayOrder = "displayOrder";
		//requestObject.status = "status";

		console.log("Request Data - " + requestObject);
		request.body = requestObject;
		findGroupCourseByProperty(request, response);
	})();
}