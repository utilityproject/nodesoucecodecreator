var productMaster = require("../models/product_master");
var logger = require("../../utils/logger");
var responseCode = require("../../utils/response_code");

function findProductByProperty(requestObject, callback) {
	var query = productMaster.findOne({});
	if (typeof requestObject.productId !== "undefined" && requestObject.productId !== null)
		query.where("productId").equals(requestObject.productId);
	if (typeof requestObject.productName !== "undefined" && requestObject.productName !== null)
		query.where("productName").equals(requestObject.productName);
	if (typeof requestObject.categoryId !== "undefined" && requestObject.categoryId !== null)
		query.where("categoryId").equals(requestObject.categoryId);
	if (typeof requestObject.groupId !== "undefined" && requestObject.groupId !== null)
		query.where("groupId").equals(requestObject.groupId);
	if (typeof requestObject.productType !== "undefined" && requestObject.productType !== null)
		query.where("productType").equals(requestObject.productType);
	if (typeof requestObject.productCode !== "undefined" && requestObject.productCode !== null)
		query.where("productCode").equals(requestObject.productCode);
	if (typeof requestObject.parentId !== "undefined" && requestObject.parentId !== null)
		query.where("parentId").equals(requestObject.parentId);
	if (typeof requestObject.mainImage !== "undefined" && requestObject.mainImage !== null)
		query.where("mainImage").equals(requestObject.mainImage);

	var responseObject = new Object();
	query.exec(function (error, data) {
		if (error) {
			logger.error(error);
			responseObject.responseCode = responseCode.MONGO_ERROR;
			callback(error, responseObject);
			return;
		}
		responseObject.responseCode = responseCode.SUCCESS;
		responseObject.responseData = data;
		callback(null, responseObject);
	});
}

module.exports = findProductByProperty;

// Unit Test Case
if (require.main === module) {
	var requestObject = new Object();
	//requestObject.productId = "productId";
	//requestObject.productName = "productName";
	//requestObject.categoryId = "categoryId";
	//requestObject.groupId = "groupId";
	//requestObject.productType = "productType";
	//requestObject.productCode = "productCode";
	//requestObject.parentId = "parentId";
	//requestObject.mainImage = "mainImage";

	findProductByProperty(requestObject, function(error, responseObject) {
		console.log("Response Code - " + responseObject.responseCode);
		if (error)
			console.log("Error - " + error);
		else
			console.log("Response Data - " + responseObject.responseData);
	});
}