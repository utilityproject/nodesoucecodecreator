var productMaster = require("../models/product_master");
var logger = require("../../utils/logger");
var responseCode = require("../../utils/response_code");

function updateProduct(requestObject, callback) {
	var updateObject = new Object();
	if(requestObject.productName)
		updateObject.productName = requestObject.productName;
	if(requestObject.categoryId)
		updateObject.categoryId = requestObject.categoryId;
	if(requestObject.groupId)
		updateObject.groupId = requestObject.groupId;
	if(requestObject.productType)
		updateObject.productType = requestObject.productType;
	if(requestObject.productCode)
		updateObject.productCode = requestObject.productCode;
	if(requestObject.parentId)
		updateObject.parentId = requestObject.parentId;
	if(requestObject.mainImage)
		updateObject.mainImage = requestObject.mainImage;
	if(requestObject.displayOrder)
		updateObject.displayOrder = requestObject.displayOrder;
	if(requestObject.status)
		updateObject.status = requestObject.status;
	updateObject.updateAt = new Date();

	var responseObject = new Object();
	var query = {productId: requestObject.productId};
	productMaster.findOneAndUpdate(query, updateObject, function (error, data) {
		if (error) {
			logger.error(error);
			responseObject.responseCode = responseCode.MONGO_ERROR;
			callback(error, responseObject);
			return;
		}
		responseObject.responseCode = responseCode.SUCCESS;
		responseObject.responseData = data;
		callback(null, responseObject);
	});
}

module.exports = updateProduct;

// Unit Test Case
if (require.main === module) {
	var requestObject = new Object();
	requestObject.productName = "";
	requestObject.categoryId = "";
	requestObject.groupId = "";
	requestObject.productType = "";
	requestObject.productCode = "";
	requestObject.parentId = "";
	requestObject.mainImage = "";
	requestObject.displayOrder = "";
	requestObject.status = "";
	console.log(requestObject);

	updateProduct(requestObject, function(error, responseObject) {
		console.log("Response Code - " + responseObject.responseCode);
		if (error)
			console.log("Error - " + error);
		else
			console.log("Response Data - " + responseObject.responseData);
	});
}