var productMaster = require("../models/product_master");
var logger = require("../../utils/logger");
var responseCode = require("../../utils/response_code");

function addProduct(requestObject, callback) {
	var newProduct = new productMaster({
		productName : requestObject.productName,
		categoryId : requestObject.categoryId,
		groupId : requestObject.groupId,
		productType : requestObject.productType,
		productCode : requestObject.productCode,
		parentId : requestObject.parentId,
		mainImage : requestObject.mainImage,
		displayOrder : requestObject.displayOrder,
		status : requestObject.status,
		createBy : requestObject.createBy
	});

	var responseObject = new Object();
	newProduct.save(function(error, data) {
		if (error) {
			logger.error(error);
			responseObject.responseCode = responseCode.MONGO_ERROR;
			callback(error, responseObject);
			return;
		}
		responseObject.responseCode = responseCode.SUCCESS;
		responseObject.responseData = data;
		callback(null, responseObject);
	});
}

module.exports = addProduct;

// Unit Test Case
if (require.main === module) {
	var requestObject = new Object();
	requestObject.productName = "";
	requestObject.categoryId = "";
	requestObject.groupId = "";
	requestObject.productType = "";
	requestObject.productCode = "";
	requestObject.parentId = "";
	requestObject.mainImage = "";
	requestObject.displayOrder = "";
	requestObject.status = "";
	requestObject.createBy = "";
	console.log(requestObject);

	addProduct(requestObject, function(error, responseObject) {
		console.log("Response Code - " + responseObject.responseCode);
		if (error)
			console.log("Error - " + error);
		else
			console.log("Response Data - " + responseObject.responseData);
	});
}