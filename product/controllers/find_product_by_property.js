var logger = require("../../utils/logger");
var productService = require("../services/index");
var responseMessage = require("../../utils/response_message");
var responseCode = require("../../utils/response_code");

function findProductByProperty(request, response, next) {
	var requestObject = request.body;
	//console.log("findProductByProperty API :- Request - %j", requestObject);

	var responseObject = new Object();
	productService.findProductByProperty(requestObject, function(error, data) {
		if(error === null) {
			responseObject.responseCode = data.responseCode;
			responseObject.responseData = data.responseData;
		} else if(data.responseCode !== responseCode.SUCCESS) {
			responseObject.responseCode = data.responseCode;
			responseObject.responseData = {};
			responseObject.responseData.message = responseMessage[data.responseCode];
		} else {
			responseObject.responseCode = data.responseCode;

			var currentRow = data.responseData;
			var currentObj = {};
			currentObj.productName = currentRow.productName;
			currentObj.categoryId = currentRow.categoryId;
			currentObj.groupId = currentRow.groupId;
			currentObj.productType = currentRow.productType;
			currentObj.productCode = currentRow.productCode;
			currentObj.parentId = currentRow.parentId;
			currentObj.mainImage = currentRow.mainImage;
			currentObj.displayOrder = currentRow.displayOrder;
			currentObj.status = currentRow.status;

			responseObject.responseData = currentObj;
		}

		logger.info("findProductByProperty API :- Response - %j", responseObject);
		response.json(responseObject);
	});
}

module.exports = findProductByProperty;

// Unit Test Case
if (require.main === module) {
	(function() {
		var request = {};
		var response = {
			json : function(result) {
				console.log(JSON.stringify(result, null, 2));
			}
		};

		var requestObject = new Object();
		//requestObject.productName = "productName";
		//requestObject.categoryId = "categoryId";
		//requestObject.groupId = "groupId";
		//requestObject.productType = "productType";
		//requestObject.productCode = "productCode";
		//requestObject.parentId = "parentId";
		//requestObject.mainImage = "mainImage";
		//requestObject.displayOrder = "displayOrder";
		//requestObject.status = "status";

		console.log("Request Data - " + requestObject);
		request.body = requestObject;
		findProductByProperty(request, response);
	})();
}