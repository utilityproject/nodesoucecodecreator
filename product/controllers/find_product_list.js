var logger = require("../../utils/logger");
var productService = require("../services/index");
var responseMessage = require("../../utils/response_message");
var responseCode = require("../../utils/response_code");

function findProductList(request, response, next) {
	var requestObject = request.body;
	//console.log("findProductList API :- Request - %j", requestObject);

	var responseObject = new Object();
	productService.findProductList(requestObject, function(error, data) {
		if(error === null) {
			responseObject.responseCode = data.responseCode;
			responseObject.responseData = data.responseData;
		} else if(data.responseCode !== responseCode.SUCCESS) {
			responseObject.responseCode = data.responseCode;
			responseObject.responseData = {};
			responseObject.responseData.message = responseMessage[data.responseCode];
		} else {
			responseObject.responseCode = data.responseCode;

			var productArr = data.responseData;
			var productList = [];
			for (var i=0, length=productArr.length; i<length; i++) {
				var currentRow = productArr[i];
				var currentObj = {};
				currentObj.productName = currentRow.productName;
				currentObj.categoryId = currentRow.categoryId;
				currentObj.groupId = currentRow.groupId;
				currentObj.productType = currentRow.productType;
				currentObj.productCode = currentRow.productCode;
				currentObj.parentId = currentRow.parentId;
				currentObj.mainImage = currentRow.mainImage;
				currentObj.displayOrder = currentRow.displayOrder;
				currentObj.status = currentRow.status;
				productList.push(currentObj);
			}
			responseObject.responseData = productList;
		}

		logger.info("findProductList API :- Response - %j", responseObject);
		response.json(responseObject);
	});
}

module.exports = findProductList;

// Unit Test Case
if (require.main === module) {
	(function() {
		var request = {};
		var response = {
			json : function(result) {
				console.log(JSON.stringify(result, null, 2));
			}
		};

		var requestObject = new Object();
		//requestObject.status = "ACTIVE";

		console.log("Request Data - " + requestObject);
		request.body = requestObject;
		findProductList(request, response);
	})();
}