var logger = require("../../utils/logger");
var productService = require("../services/index");
var responseMessage = require("../../utils/response_message");
var responseCode = require("../../utils/response_code");

function addProduct(request, response, next) {
	var requestObject = request.body;
	//console.log("addProduct API :- Request - %j", requestObject);

	var responseObject = new Object();
	productService.addProduct(requestObject, function(error, data) {
		if(error === null) {
			responseObject.responseCode = data.responseCode;
			responseObject.responseData = data.responseData;
		} else if(data.responseCode !== responseCode.SUCCESS) {
			responseObject.responseCode = data.responseCode;
			responseObject.responseData = {};
			responseObject.responseData.message = responseMessage[data.responseCode];
		} else {
			responseObject.responseCode = data.responseCode;
			responseObject.responseData = data.responseData;
		}

		logger.info("addProduct API :- Response - %j", responseObject);
		response.json(responseObject);
	});
}

module.exports = addProduct;

// Unit Test Case
if (require.main === module) {
	(function() {
		var request = {};
		var response = {
			json : function(result) {
				console.log(JSON.stringify(result, null, 2));
			}
		};

		var requestObject = new Object();
		requestObject.productName = "";
		requestObject.categoryId = "";
		requestObject.groupId = "";
		requestObject.productType = "";
		requestObject.productCode = "";
		requestObject.parentId = "";
		requestObject.mainImage = "";
		requestObject.displayOrder = "";
		requestObject.status = "";
		requestObject.createBy = "";

		console.log("Request Data - " + requestObject);
		request.body = requestObject;
		addProduct(request, response);
	})();
}