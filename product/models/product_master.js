require("../../utils/mongo_util");
var mongoose = require("mongoose");
var autoIncrement = require("mongoose-auto-increment");

var ProductMasterSchema = new mongoose.Schema({
	productId: {type: Number, unique: true},
	productName: {type: String},
	categoryId: {type: Number},
	groupId: {type: Number},
	productType: {type: String},
	productCode: {type: String},
	parentId: {type: Number},
	mainImage: {type: String},
	displayOrder: {type: Number},
	status: {type: String, enum: ["ACTIVE", "INACTIVE"]},
	createBy: Number,
	createAt: {type: Date, default: Date.now},
	updateAt: {type: Date}
});

module.exports = mongoose.model("ProductMaster", ProductMasterSchema, "product_master");

ProductMasterSchema.plugin(autoIncrement.plugin, {
	model: "ProductMaster",
	field: "productId",
	startAt: 1,
	incrementBy: 1
});