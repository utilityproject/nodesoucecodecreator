require("../../utils/mongo_util");
var mongoose = require("mongoose");
var autoIncrement = require("mongoose-auto-increment");

var FtSpecialsMasterSchema = new mongoose.Schema({
	tutorialId: {type: Number, unique: true},
	groupId: {type: Number},
	courseId: {type: Number},
	sectionId: {type: Number},
	categoryId: {type: Number},
	menuId: {type: Number},
	tutorialDescription: {type: String},
	views: {type: String},
	likes: {type: String},
	comments: {type: String},
	displayOrder: {type: Number},
	status: {type: String, enum: ["ACTIVE", "INACTIVE"]},
	createBy: Number,
	createAt: {type: Date, default: Date.now},
	updateAt: {type: Date}
});

module.exports = mongoose.model("FtSpecialsMaster", FtSpecialsMasterSchema, "ft_specials_master");

FtSpecialsMasterSchema.plugin(autoIncrement.plugin, {
	model: "FtSpecialsMaster",
	field: "tutorialId",
	startAt: 1,
	incrementBy: 1
});