var logger = require("../../utils/logger");
var ftSpecialsService = require("../services/index");
var responseMessage = require("../../utils/response_message");
var responseCode = require("../../utils/response_code");

function findFtSpecialsList(request, response, next) {
	var requestObject = request.body;
	//console.log("findFtSpecialsList API :- Request - %j", requestObject);

	var responseObject = new Object();
	ftSpecialsService.findFtSpecialsList(requestObject, function(error, data) {
		if(error === null) {
			responseObject.responseCode = data.responseCode;
			responseObject.responseData = data.responseData;
		} else if(data.responseCode !== responseCode.SUCCESS) {
			responseObject.responseCode = data.responseCode;
			responseObject.responseData = {};
			responseObject.responseData.message = responseMessage[data.responseCode];
		} else {
			responseObject.responseCode = data.responseCode;

			var ftSpecialsArr = data.responseData;
			var ftSpecialsList = [];
			for (var i=0, length=ftSpecialsArr.length; i<length; i++) {
				var currentRow = ftSpecialsArr[i];
				var currentObj = {};
				currentObj.groupId = currentRow.groupId;
				currentObj.courseId = currentRow.courseId;
				currentObj.sectionId = currentRow.sectionId;
				currentObj.categoryId = currentRow.categoryId;
				currentObj.menuId = currentRow.menuId;
				currentObj.tutorialDescription = currentRow.tutorialDescription;
				currentObj.views = currentRow.views;
				currentObj.likes = currentRow.likes;
				currentObj.comments = currentRow.comments;
				currentObj.displayOrder = currentRow.displayOrder;
				currentObj.status = currentRow.status;
				ftSpecialsList.push(currentObj);
			}
			responseObject.responseData = ftSpecialsList;
		}

		logger.info("findFtSpecialsList API :- Response - %j", responseObject);
		response.json(responseObject);
	});
}

module.exports = findFtSpecialsList;

// Unit Test Case
if (require.main === module) {
	(function() {
		var request = {};
		var response = {
			json : function(result) {
				console.log(JSON.stringify(result, null, 2));
			}
		};

		var requestObject = new Object();
		//requestObject.status = "ACTIVE";

		console.log("Request Data - " + requestObject);
		request.body = requestObject;
		findFtSpecialsList(request, response);
	})();
}