var addFtSpecialsApi = require("./add_ft_specials");
var findFtSpecialsByPropertyApi = require("./find_ft_specials_by_property");
var findFtSpecialsListApi = require("./find_ft_specials_list");
var updateFtSpecialsApi = require("./update_ft_specials");
var updateFtSpecialsDisplayOrderApi = require("./update_ft_specials_display_order");
// Require

module.exports = {
	addFtSpecials : addFtSpecialsApi,
	findFtSpecialsByProperty : findFtSpecialsByPropertyApi,
	findFtSpecialsList : findFtSpecialsListApi,
	updateFtSpecials : updateFtSpecialsApi,
	updateFtSpecialsDisplayOrder : updateFtSpecialsDisplayOrderApi
// Export
};