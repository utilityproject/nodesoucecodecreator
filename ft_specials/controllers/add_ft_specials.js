var logger = require("../../utils/logger");
var ftSpecialsService = require("../services/index");
var responseMessage = require("../../utils/response_message");
var responseCode = require("../../utils/response_code");

function addFtSpecials(request, response, next) {
	var requestObject = request.body;
	//console.log("addFtSpecials API :- Request - %j", requestObject);

	var responseObject = new Object();
	ftSpecialsService.addFtSpecials(requestObject, function(error, data) {
		if(error === null) {
			responseObject.responseCode = data.responseCode;
			responseObject.responseData = data.responseData;
		} else if(data.responseCode !== responseCode.SUCCESS) {
			responseObject.responseCode = data.responseCode;
			responseObject.responseData = {};
			responseObject.responseData.message = responseMessage[data.responseCode];
		} else {
			responseObject.responseCode = data.responseCode;
			responseObject.responseData = data.responseData;
		}

		logger.info("addFtSpecials API :- Response - %j", responseObject);
		response.json(responseObject);
	});
}

module.exports = addFtSpecials;

// Unit Test Case
if (require.main === module) {
	(function() {
		var request = {};
		var response = {
			json : function(result) {
				console.log(JSON.stringify(result, null, 2));
			}
		};

		var requestObject = new Object();
		requestObject.groupId = "";
		requestObject.courseId = "";
		requestObject.sectionId = "";
		requestObject.categoryId = "";
		requestObject.menuId = "";
		requestObject.tutorialDescription = "";
		requestObject.views = "";
		requestObject.likes = "";
		requestObject.comments = "";
		requestObject.displayOrder = "";
		requestObject.status = "";
		requestObject.createBy = "";

		console.log("Request Data - " + requestObject);
		request.body = requestObject;
		addFtSpecials(request, response);
	})();
}