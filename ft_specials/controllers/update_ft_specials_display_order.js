var logger = require("../../utils/logger");
var ftSpecialsService = require("../services/index");
var responseMessage = require("../../utils/response_message");
var responseCode = require("../../utils/response_code");

function updateFtSpecialsDisplayOrder(request, response, next) {
	var requestObject = request.body;
	//console.log("updateFtSpecialsDisplayOrder API :- Request - %j", requestObject);

	var responseObject = new Object();
	ftSpecialsService.updateFtSpecialsDisplayOrder(requestObject, function(error, data) {
		if(error === null) {
			responseObject.responseCode = data.responseCode;
			responseObject.responseData = data.responseData;
		} else if(data.responseCode !== responseCode.SUCCESS) {
			responseObject.responseCode = data.responseCode;
			responseObject.responseData = {};
			responseObject.responseData.message = responseMessage[data.responseCode];
		} else {
			responseObject.responseCode = data.responseCode;
			responseObject.responseData = data.responseData;
		}

		logger.info("updateFtSpecialsDisplayOrder API :- Response - %j", responseObject);
		response.json(responseObject);
	});
}

module.exports = updateFtSpecialsDisplayOrder;