var logger = require("../../utils/logger");
var ftSpecialsService = require("../services/index");
var responseMessage = require("../../utils/response_message");
var responseCode = require("../../utils/response_code");

function findFtSpecialsByProperty(request, response, next) {
	var requestObject = request.body;
	//console.log("findFtSpecialsByProperty API :- Request - %j", requestObject);

	var responseObject = new Object();
	ftSpecialsService.findFtSpecialsByProperty(requestObject, function(error, data) {
		if(error === null) {
			responseObject.responseCode = data.responseCode;
			responseObject.responseData = data.responseData;
		} else if(data.responseCode !== responseCode.SUCCESS) {
			responseObject.responseCode = data.responseCode;
			responseObject.responseData = {};
			responseObject.responseData.message = responseMessage[data.responseCode];
		} else {
			responseObject.responseCode = data.responseCode;

			var currentRow = data.responseData;
			var currentObj = {};
			currentObj.groupId = currentRow.groupId;
			currentObj.courseId = currentRow.courseId;
			currentObj.sectionId = currentRow.sectionId;
			currentObj.categoryId = currentRow.categoryId;
			currentObj.menuId = currentRow.menuId;
			currentObj.tutorialDescription = currentRow.tutorialDescription;
			currentObj.views = currentRow.views;
			currentObj.likes = currentRow.likes;
			currentObj.comments = currentRow.comments;
			currentObj.displayOrder = currentRow.displayOrder;
			currentObj.status = currentRow.status;

			responseObject.responseData = currentObj;
		}

		logger.info("findFtSpecialsByProperty API :- Response - %j", responseObject);
		response.json(responseObject);
	});
}

module.exports = findFtSpecialsByProperty;

// Unit Test Case
if (require.main === module) {
	(function() {
		var request = {};
		var response = {
			json : function(result) {
				console.log(JSON.stringify(result, null, 2));
			}
		};

		var requestObject = new Object();
		//requestObject.groupId = "groupId";
		//requestObject.courseId = "courseId";
		//requestObject.sectionId = "sectionId";
		//requestObject.categoryId = "categoryId";
		//requestObject.menuId = "menuId";
		//requestObject.tutorialDescription = "tutorialDescription";
		//requestObject.views = "views";
		//requestObject.likes = "likes";
		//requestObject.comments = "comments";
		//requestObject.displayOrder = "displayOrder";
		//requestObject.status = "status";

		console.log("Request Data - " + requestObject);
		request.body = requestObject;
		findFtSpecialsByProperty(request, response);
	})();
}