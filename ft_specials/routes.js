var obj = require('./controllers/index');

module.exports = function(app) {
	app.post("/addFtSpecials", obj.addFtSpecials);
	app.post("/updateFtSpecials", obj.updateFtSpecials);
	app.post("/findFtSpecialsByProperty", obj.findFtSpecialsByProperty);
	app.post("/findFtSpecialsList", obj.findFtSpecialsList);
	app.post("/updateFtSpecialsDisplayOrder", obj.updateFtSpecialsDisplayOrder);
// Routes
};