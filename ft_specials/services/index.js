var addFtSpecialsService = require("./add_ft_specials");
var findFtSpecialsByPropertyService = require("./find_ft_specials_by_property");
var findFtSpecialsListService = require("./find_ft_specials_list");
var updateFtSpecialsService = require("./update_ft_specials");
var updateFtSpecialsDisplayOrderService = require("./update_ft_specials_display_order");
// Require

module.exports = {
	addFtSpecials : addFtSpecialsService,
	findFtSpecialsByProperty : findFtSpecialsByPropertyService,
	findFtSpecialsList : findFtSpecialsListService,
	updateFtSpecials : updateFtSpecialsService,
	updateFtSpecialsDisplayOrder : updateFtSpecialsDisplayOrderService
// Export
};