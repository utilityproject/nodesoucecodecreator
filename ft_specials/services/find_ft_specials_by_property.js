var ftSpecialsMaster = require("../models/ft_specials_master");
var logger = require("../../utils/logger");
var responseCode = require("../../utils/response_code");

function findFtSpecialsByProperty(requestObject, callback) {
	var query = ftSpecialsMaster.findOne({});
	if (typeof requestObject.tutorialId !== "undefined" && requestObject.tutorialId !== null)
		query.where("tutorialId").equals(requestObject.tutorialId);
	if (typeof requestObject.groupId !== "undefined" && requestObject.groupId !== null)
		query.where("groupId").equals(requestObject.groupId);
	if (typeof requestObject.courseId !== "undefined" && requestObject.courseId !== null)
		query.where("courseId").equals(requestObject.courseId);
	if (typeof requestObject.sectionId !== "undefined" && requestObject.sectionId !== null)
		query.where("sectionId").equals(requestObject.sectionId);
	if (typeof requestObject.categoryId !== "undefined" && requestObject.categoryId !== null)
		query.where("categoryId").equals(requestObject.categoryId);
	if (typeof requestObject.menuId !== "undefined" && requestObject.menuId !== null)
		query.where("menuId").equals(requestObject.menuId);
	if (typeof requestObject.tutorialDescription !== "undefined" && requestObject.tutorialDescription !== null)
		query.where("tutorialDescription").equals(requestObject.tutorialDescription);
	if (typeof requestObject.views !== "undefined" && requestObject.views !== null)
		query.where("views").equals(requestObject.views);
	if (typeof requestObject.likes !== "undefined" && requestObject.likes !== null)
		query.where("likes").equals(requestObject.likes);
	if (typeof requestObject.comments !== "undefined" && requestObject.comments !== null)
		query.where("comments").equals(requestObject.comments);

	var responseObject = new Object();
	query.exec(function (error, data) {
		if (error) {
			logger.error(error);
			responseObject.responseCode = responseCode.MONGO_ERROR;
			callback(error, responseObject);
			return;
		}
		responseObject.responseCode = responseCode.SUCCESS;
		responseObject.responseData = data;
		callback(null, responseObject);
	});
}

module.exports = findFtSpecialsByProperty;

// Unit Test Case
if (require.main === module) {
	var requestObject = new Object();
	//requestObject.tutorialId = "tutorialId";
	//requestObject.groupId = "groupId";
	//requestObject.courseId = "courseId";
	//requestObject.sectionId = "sectionId";
	//requestObject.categoryId = "categoryId";
	//requestObject.menuId = "menuId";
	//requestObject.tutorialDescription = "tutorialDescription";
	//requestObject.views = "views";
	//requestObject.likes = "likes";
	//requestObject.comments = "comments";

	findFtSpecialsByProperty(requestObject, function(error, responseObject) {
		console.log("Response Code - " + responseObject.responseCode);
		if (error)
			console.log("Error - " + error);
		else
			console.log("Response Data - " + responseObject.responseData);
	});
}