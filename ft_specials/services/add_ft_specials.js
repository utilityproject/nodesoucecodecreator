var ftSpecialsMaster = require("../models/ft_specials_master");
var logger = require("../../utils/logger");
var responseCode = require("../../utils/response_code");

function addFtSpecials(requestObject, callback) {
	var newFtSpecials = new ftSpecialsMaster({
		groupId : requestObject.groupId,
		courseId : requestObject.courseId,
		sectionId : requestObject.sectionId,
		categoryId : requestObject.categoryId,
		menuId : requestObject.menuId,
		tutorialDescription : requestObject.tutorialDescription,
		views : requestObject.views,
		likes : requestObject.likes,
		comments : requestObject.comments,
		displayOrder : requestObject.displayOrder,
		status : requestObject.status,
		createBy : requestObject.createBy
	});

	var responseObject = new Object();
	newFtSpecials.save(function(error, data) {
		if (error) {
			logger.error(error);
			responseObject.responseCode = responseCode.MONGO_ERROR;
			callback(error, responseObject);
			return;
		}
		responseObject.responseCode = responseCode.SUCCESS;
		responseObject.responseData = data;
		callback(null, responseObject);
	});
}

module.exports = addFtSpecials;

// Unit Test Case
if (require.main === module) {
	var requestObject = new Object();
	requestObject.groupId = "";
	requestObject.courseId = "";
	requestObject.sectionId = "";
	requestObject.categoryId = "";
	requestObject.menuId = "";
	requestObject.tutorialDescription = "";
	requestObject.views = "";
	requestObject.likes = "";
	requestObject.comments = "";
	requestObject.displayOrder = "";
	requestObject.status = "";
	requestObject.createBy = "";
	console.log(requestObject);

	addFtSpecials(requestObject, function(error, responseObject) {
		console.log("Response Code - " + responseObject.responseCode);
		if (error)
			console.log("Error - " + error);
		else
			console.log("Response Data - " + responseObject.responseData);
	});
}