var ftSpecialsMaster = require("../models/ft_specials_master");
var logger = require("../../utils/logger");
var responseCode = require("../../utils/response_code");

function updateFtSpecials(requestObject, callback) {
	var updateObject = new Object();
	if(requestObject.groupId)
		updateObject.groupId = requestObject.groupId;
	if(requestObject.courseId)
		updateObject.courseId = requestObject.courseId;
	if(requestObject.sectionId)
		updateObject.sectionId = requestObject.sectionId;
	if(requestObject.categoryId)
		updateObject.categoryId = requestObject.categoryId;
	if(requestObject.menuId)
		updateObject.menuId = requestObject.menuId;
	if(requestObject.tutorialDescription)
		updateObject.tutorialDescription = requestObject.tutorialDescription;
	if(requestObject.views)
		updateObject.views = requestObject.views;
	if(requestObject.likes)
		updateObject.likes = requestObject.likes;
	if(requestObject.comments)
		updateObject.comments = requestObject.comments;
	if(requestObject.displayOrder)
		updateObject.displayOrder = requestObject.displayOrder;
	if(requestObject.status)
		updateObject.status = requestObject.status;
	updateObject.updateAt = new Date();

	var responseObject = new Object();
	var query = {tutorialId: requestObject.tutorialId};
	ftSpecialsMaster.findOneAndUpdate(query, updateObject, function (error, data) {
		if (error) {
			logger.error(error);
			responseObject.responseCode = responseCode.MONGO_ERROR;
			callback(error, responseObject);
			return;
		}
		responseObject.responseCode = responseCode.SUCCESS;
		responseObject.responseData = data;
		callback(null, responseObject);
	});
}

module.exports = updateFtSpecials;

// Unit Test Case
if (require.main === module) {
	var requestObject = new Object();
	requestObject.groupId = "";
	requestObject.courseId = "";
	requestObject.sectionId = "";
	requestObject.categoryId = "";
	requestObject.menuId = "";
	requestObject.tutorialDescription = "";
	requestObject.views = "";
	requestObject.likes = "";
	requestObject.comments = "";
	requestObject.displayOrder = "";
	requestObject.status = "";
	console.log(requestObject);

	updateFtSpecials(requestObject, function(error, responseObject) {
		console.log("Response Code - " + responseObject.responseCode);
		if (error)
			console.log("Error - " + error);
		else
			console.log("Response Data - " + responseObject.responseData);
	});
}