require("../../utils/mongo_util");
var mongoose = require("mongoose");
var autoIncrement = require("mongoose-auto-increment");

var ProductCategoryMasterSchema = new mongoose.Schema({
	categoryId: {type: Number, unique: true},
	categoryName: {type: String},
	groupId: {type: Number},
	parentCategoryId: {type: Number},
	categoryType: {type: String},
	seoData: {
		urlName: String,
		metaTitle: String,
		metaKeywords: String,
		metaDescription: String
	},
	displayOrder: {type: Number},
	status: {type: String, enum: ["ACTIVE", "INACTIVE"]},
	createBy: Number,
	createAt: {type: Date, default: Date.now},
	updateAt: {type: Date}
});

module.exports = mongoose.model("ProductCategoryMaster", ProductCategoryMasterSchema, "product_category_master");

ProductCategoryMasterSchema.plugin(autoIncrement.plugin, {
	model: "ProductCategoryMaster",
	field: "categoryId",
	startAt: 1,
	incrementBy: 1
});