var logger = require("../../utils/logger");
var productCategoryService = require("../services/index");
var responseMessage = require("../../utils/response_message");
var responseCode = require("../../utils/response_code");

function updateProductCategory(request, response, next) {
	var requestObject = request.body;
	//console.log("updateProductCategory API :- Request - %j", requestObject);

	var responseObject = new Object();
	productCategoryService.updateProductCategory(requestObject, function(error, data) {
		if(error === null) {
			responseObject.responseCode = data.responseCode;
			responseObject.responseData = data.responseData;
		} else if(data.responseCode !== responseCode.SUCCESS) {
			responseObject.responseCode = data.responseCode;
			responseObject.responseData = {};
			responseObject.responseData.message = responseMessage[data.responseCode];
		} else {
			responseObject.responseCode = data.responseCode;
			responseObject.responseData = data.responseData;
		}

		logger.info("updateProductCategory API :- Response - %j", responseObject);
		response.json(responseObject);
	});
}

module.exports = updateProductCategory;

// Unit Test Case
if (require.main === module) {
	(function() {
		var request = {};
		var response = {
			json : function(result) {
				console.log(JSON.stringify(result, null, 2));
			}
		};

		var requestObject = new Object();
		requestObject.categoryName = "";
		requestObject.groupId = "";
		requestObject.parentCategoryId = "";
		requestObject.categoryType = "";
		requestObject.urlName = "";
		requestObject.metaTitle = "";
		requestObject.metaKeywords = "";
		requestObject.metaDescription = "";
		requestObject.displayOrder = "";
		requestObject.status = "";
		requestObject.createBy = "";

		console.log("Request Data - " + requestObject);
		request.body = requestObject;
		updateProductCategory(request, response);
	})();
}