var logger = require("../../utils/logger");
var productCategoryService = require("../services/index");
var responseMessage = require("../../utils/response_message");
var responseCode = require("../../utils/response_code");

function findProductCategoryList(request, response, next) {
	var requestObject = request.body;
	//console.log("findProductCategoryList API :- Request - %j", requestObject);

	var responseObject = new Object();
	productCategoryService.findProductCategoryList(requestObject, function(error, data) {
		if(error === null) {
			responseObject.responseCode = data.responseCode;
			responseObject.responseData = data.responseData;
		} else if(data.responseCode !== responseCode.SUCCESS) {
			responseObject.responseCode = data.responseCode;
			responseObject.responseData = {};
			responseObject.responseData.message = responseMessage[data.responseCode];
		} else {
			responseObject.responseCode = data.responseCode;

			var productCategoryArr = data.responseData;
			var productCategoryList = [];
			for (var i=0, length=productCategoryArr.length; i<length; i++) {
				var currentRow = productCategoryArr[i];
				var currentObj = {};
				currentObj.categoryName = currentRow.categoryName;
				currentObj.groupId = currentRow.groupId;
				currentObj.parentCategoryId = currentRow.parentCategoryId;
				currentObj.categoryType = currentRow.categoryType;
				currentObj.urlName = currentRow.seoData.urlName;
				currentObj.metaTitle = currentRow.seoData.metaTitle;
				currentObj.metaKeywords = currentRow.seoData.metaKeywords;
				currentObj.metaDescription = currentRow.seoData.metaDescription;
				currentObj.displayOrder = currentRow.displayOrder;
				currentObj.status = currentRow.status;
				productCategoryList.push(currentObj);
			}
			responseObject.responseData = productCategoryList;
		}

		logger.info("findProductCategoryList API :- Response - %j", responseObject);
		response.json(responseObject);
	});
}

module.exports = findProductCategoryList;

// Unit Test Case
if (require.main === module) {
	(function() {
		var request = {};
		var response = {
			json : function(result) {
				console.log(JSON.stringify(result, null, 2));
			}
		};

		var requestObject = new Object();
		//requestObject.status = "ACTIVE";

		console.log("Request Data - " + requestObject);
		request.body = requestObject;
		findProductCategoryList(request, response);
	})();
}