var productCategoryMaster = require("../models/product_category_master");
var logger = require("../../utils/logger");
var responseCode = require("../../utils/response_code");

function addProductCategory(requestObject, callback) {
	var newProductCategory = new productCategoryMaster({
		categoryName : requestObject.categoryName,
		groupId : requestObject.groupId,
		parentCategoryId : requestObject.parentCategoryId,
		categoryType : requestObject.categoryType,
		seoData : {
			urlName : requestObject.urlName,
			metaTitle : requestObject.metaTitle,
			metaKeywords : requestObject.metaKeywords,
			metaDescription : requestObject.metaDescription
		},
		displayOrder : requestObject.displayOrder,
		status : requestObject.status,
		createBy : requestObject.createBy
	});

	var responseObject = new Object();
	newProductCategory.save(function(error, data) {
		if (error) {
			logger.error(error);
			responseObject.responseCode = responseCode.MONGO_ERROR;
			callback(error, responseObject);
			return;
		}
		responseObject.responseCode = responseCode.SUCCESS;
		responseObject.responseData = data;
		callback(null, responseObject);
	});
}

module.exports = addProductCategory;

// Unit Test Case
if (require.main === module) {
	var requestObject = new Object();
	requestObject.categoryName = "";
	requestObject.groupId = "";
	requestObject.parentCategoryId = "";
	requestObject.categoryType = "";
	requestObject.urlName = "";
	requestObject.metaTitle = "";
	requestObject.metaKeywords = "";
	requestObject.metaDescription = "";
	requestObject.displayOrder = "";
	requestObject.status = "";
	requestObject.createBy = "";
	console.log(requestObject);

	addProductCategory(requestObject, function(error, responseObject) {
		console.log("Response Code - " + responseObject.responseCode);
		if (error)
			console.log("Error - " + error);
		else
			console.log("Response Data - " + responseObject.responseData);
	});
}