var productCategoryMaster = require("../models/product_category_master");
var logger = require("../../utils/logger");
var responseCode = require("../../utils/response_code");

function findProductCategoryByProperty(requestObject, callback) {
	var query = productCategoryMaster.findOne({});
	if (typeof requestObject.categoryId !== "undefined" && requestObject.categoryId !== null)
		query.where("categoryId").equals(requestObject.categoryId);
	if (typeof requestObject.categoryName !== "undefined" && requestObject.categoryName !== null)
		query.where("categoryName").equals(requestObject.categoryName);
	if (typeof requestObject.groupId !== "undefined" && requestObject.groupId !== null)
		query.where("groupId").equals(requestObject.groupId);
	if (typeof requestObject.parentCategoryId !== "undefined" && requestObject.parentCategoryId !== null)
		query.where("parentCategoryId").equals(requestObject.parentCategoryId);
	if (typeof requestObject.categoryType !== "undefined" && requestObject.categoryType !== null)
		query.where("categoryType").equals(requestObject.categoryType);
	if (typeof requestObject.seoData !== "undefined" && requestObject.seoData !== null)
		query.where("seoData").equals(requestObject.seoData);

	var responseObject = new Object();
	query.exec(function (error, data) {
		if (error) {
			logger.error(error);
			responseObject.responseCode = responseCode.MONGO_ERROR;
			callback(error, responseObject);
			return;
		}
		responseObject.responseCode = responseCode.SUCCESS;
		responseObject.responseData = data;
		callback(null, responseObject);
	});
}

module.exports = findProductCategoryByProperty;

// Unit Test Case
if (require.main === module) {
	var requestObject = new Object();
	//requestObject.categoryId = "categoryId";
	//requestObject.categoryName = "categoryName";
	//requestObject.groupId = "groupId";
	//requestObject.parentCategoryId = "parentCategoryId";
	//requestObject.categoryType = "categoryType";
	//requestObject.seoData = "seoData";

	findProductCategoryByProperty(requestObject, function(error, responseObject) {
		console.log("Response Code - " + responseObject.responseCode);
		if (error)
			console.log("Error - " + error);
		else
			console.log("Response Data - " + responseObject.responseData);
	});
}