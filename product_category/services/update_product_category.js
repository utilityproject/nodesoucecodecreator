var productCategoryMaster = require("../models/product_category_master");
var logger = require("../../utils/logger");
var responseCode = require("../../utils/response_code");

function updateProductCategory(requestObject, callback) {
	var updateObject = new Object();
	if(requestObject.categoryName)
		updateObject.categoryName = requestObject.categoryName;
	if(requestObject.groupId)
		updateObject.groupId = requestObject.groupId;
	if(requestObject.parentCategoryId)
		updateObject.parentCategoryId = requestObject.parentCategoryId;
	if(requestObject.categoryType)
		updateObject.categoryType = requestObject.categoryType;
	if(requestObject.urlName)
		updateObject["seoData.urlName"] = requestObject.urlName;
	if(requestObject.metaTitle)
		updateObject["seoData.metaTitle"] = requestObject.metaTitle;
	if(requestObject.metaKeywords)
		updateObject["seoData.metaKeywords"] = requestObject.metaKeywords;
	if(requestObject.metaDescription)
		updateObject["seoData.metaDescription"] = requestObject.metaDescription;
	if(requestObject.displayOrder)
		updateObject.displayOrder = requestObject.displayOrder;
	if(requestObject.status)
		updateObject.status = requestObject.status;
	updateObject.updateAt = new Date();

	var responseObject = new Object();
	var query = {categoryId: requestObject.categoryId};
	productCategoryMaster.findOneAndUpdate(query, updateObject, function (error, data) {
		if (error) {
			logger.error(error);
			responseObject.responseCode = responseCode.MONGO_ERROR;
			callback(error, responseObject);
			return;
		}
		responseObject.responseCode = responseCode.SUCCESS;
		responseObject.responseData = data;
		callback(null, responseObject);
	});
}

module.exports = updateProductCategory;

// Unit Test Case
if (require.main === module) {
	var requestObject = new Object();
	requestObject.categoryName = "";
	requestObject.groupId = "";
	requestObject.parentCategoryId = "";
	requestObject.categoryType = "";
	requestObject.urlName = "";
	requestObject.metaTitle = "";
	requestObject.metaKeywords = "";
	requestObject.metaDescription = "";
	requestObject.displayOrder = "";
	requestObject.status = "";
	console.log(requestObject);

	updateProductCategory(requestObject, function(error, responseObject) {
		console.log("Response Code - " + responseObject.responseCode);
		if (error)
			console.log("Error - " + error);
		else
			console.log("Response Data - " + responseObject.responseData);
	});
}