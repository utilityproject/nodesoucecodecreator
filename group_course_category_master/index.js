var obj = require('./controllers/index');

module.exports = function(app) {
	app.post("/addGroupCourseCategory", obj.addGroupCourseCategory);
	app.post("/updateGroupCourseCategory", obj.updateGroupCourseCategory);
	app.post("/findGroupCourseCategoryByProperty", obj.findGroupCourseCategoryByProperty);
	app.post("/findGroupCourseCategoryList", obj.findGroupCourseCategoryList);
	app.post("/updateGroupCourseCategoryDisplayOrder", obj.updateGroupCourseCategoryDisplayOrder);
// Routes
};