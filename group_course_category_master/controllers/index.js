var addGroupCourseCategoryApi = require("./add_group_course_category");
var findGroupCourseCategoryByPropertyApi = require("./find_group_course_category_by_property");
var findGroupCourseCategoryListApi = require("./find_group_course_category_list");
var updateGroupCourseCategoryApi = require("./update_group_course_category");
var updateGroupCourseCategoryDisplayOrderApi = require("./update_group_course_category_display_order");
// Require

module.exports = {
	addGroupCourseCategory : addGroupCourseCategoryApi,
	findGroupCourseCategoryByProperty : findGroupCourseCategoryByPropertyApi,
	findGroupCourseCategoryList : findGroupCourseCategoryListApi,
	updateGroupCourseCategory : updateGroupCourseCategoryApi,
	updateGroupCourseCategoryDisplayOrder : updateGroupCourseCategoryDisplayOrderApi
// Export
};