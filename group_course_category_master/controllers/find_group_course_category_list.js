var logger = require("../../utils/logger");
var groupCourseCategoryService = require("../services/index");
var responseMessage = require("../../utils/response_message");
var responseCode = require("../../utils/response_code");

function findGroupCourseCategoryList(request, response, next) {
	var requestObject = request.body;
	//console.log("findGroupCourseCategoryList API :- Request - %j", requestObject);

	var responseObject = new Object();
	groupCourseCategoryService.findGroupCourseCategoryList(requestObject, function(error, data) {
		if(error === null) {
			responseObject.responseCode = data.responseCode;
			responseObject.responseData = data.responseData;
		} else if(data.responseCode !== responseCode.SUCCESS) {
			responseObject.responseCode = data.responseCode;
			responseObject.responseData = {};
			responseObject.responseData.message = responseMessage[data.responseCode];
		} else {
			responseObject.responseCode = data.responseCode;

			var groupCourseCategoryArr = data.responseData;
			var groupCourseCategoryList = [];
			for (var i=0, length=groupCourseCategoryArr.length; i<length; i++) {
				var currentRow = groupCourseCategoryArr[i];
				var currentObj = {};
				currentObj.courseId = currentRow.courseId;
				currentObj.categoryName = currentRow.categoryName;
				currentObj.categoryType = currentRow.categoryType;
				currentObj.refId = currentRow.refId;
				currentObj.displayOrder = currentRow.displayOrder;
				currentObj.status = currentRow.status;
				groupCourseCategoryList.push(currentObj);
			}
			responseObject.responseData = groupCourseCategoryList;
		}

		logger.info("findGroupCourseCategoryList API :- Response - %j", responseObject);
		response.json(responseObject);
	});
}

module.exports = findGroupCourseCategoryList;

// Unit Test Case
if (require.main === module) {
	(function() {
		var request = {};
		var response = {
			json : function(result) {
				console.log(JSON.stringify(result, null, 2));
			}
		};

		var requestObject = new Object();
		//requestObject.status = "ACTIVE";

		console.log("Request Data - " + requestObject);
		request.body = requestObject;
		findGroupCourseCategoryList(request, response);
	})();
}