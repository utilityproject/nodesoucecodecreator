var logger = require("../../utils/logger");
var groupCourseCategoryService = require("../services/index");
var responseMessage = require("../../utils/response_message");
var responseCode = require("../../utils/response_code");

function findGroupCourseCategoryByProperty(request, response, next) {
	var requestObject = request.body;
	//console.log("findGroupCourseCategoryByProperty API :- Request - %j", requestObject);

	var responseObject = new Object();
	groupCourseCategoryService.findGroupCourseCategoryByProperty(requestObject, function(error, data) {
		if(error === null) {
			responseObject.responseCode = data.responseCode;
			responseObject.responseData = data.responseData;
		} else if(data.responseCode !== responseCode.SUCCESS) {
			responseObject.responseCode = data.responseCode;
			responseObject.responseData = {};
			responseObject.responseData.message = responseMessage[data.responseCode];
		} else {
			responseObject.responseCode = data.responseCode;

			var currentRow = data.responseData;
			var currentObj = {};
			currentObj.courseId = currentRow.courseId;
			currentObj.categoryName = currentRow.categoryName;
			currentObj.categoryType = currentRow.categoryType;
			currentObj.refId = currentRow.refId;
			currentObj.displayOrder = currentRow.displayOrder;
			currentObj.status = currentRow.status;

			responseObject.responseData = currentObj;
		}

		logger.info("findGroupCourseCategoryByProperty API :- Response - %j", responseObject);
		response.json(responseObject);
	});
}

module.exports = findGroupCourseCategoryByProperty;

// Unit Test Case
if (require.main === module) {
	(function() {
		var request = {};
		var response = {
			json : function(result) {
				console.log(JSON.stringify(result, null, 2));
			}
		};

		var requestObject = new Object();
		//requestObject.courseId = "courseId";
		//requestObject.categoryName = "categoryName";
		//requestObject.categoryType = "categoryType";
		//requestObject.refId = "refId";
		//requestObject.displayOrder = "displayOrder";
		//requestObject.status = "status";

		console.log("Request Data - " + requestObject);
		request.body = requestObject;
		findGroupCourseCategoryByProperty(request, response);
	})();
}