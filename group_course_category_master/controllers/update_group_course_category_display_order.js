var logger = require("../../utils/logger");
var groupCourseCategoryService = require("../services/index");
var responseMessage = require("../../utils/response_message");
var responseCode = require("../../utils/response_code");

function updateGroupCourseCategoryDisplayOrder(request, response, next) {
	var requestObject = request.body;
	//console.log("updateGroupCourseCategoryDisplayOrder API :- Request - %j", requestObject);

	var responseObject = new Object();
	groupCourseCategoryService.updateGroupCourseCategoryDisplayOrder(requestObject, function(error, data) {
		if(error === null) {
			responseObject.responseCode = data.responseCode;
			responseObject.responseData = data.responseData;
		} else if(data.responseCode !== responseCode.SUCCESS) {
			responseObject.responseCode = data.responseCode;
			responseObject.responseData = {};
			responseObject.responseData.message = responseMessage[data.responseCode];
		} else {
			responseObject.responseCode = data.responseCode;
			responseObject.responseData = data.responseData;
		}

		logger.info("updateGroupCourseCategoryDisplayOrder API :- Response - %j", responseObject);
		response.json(responseObject);
	});
}

module.exports = updateGroupCourseCategoryDisplayOrder;