var groupCourseCategoryMaster = require("../models/group_course_category_master");
var logger = require("../../utils/logger");
var responseCode = require("../../utils/response_code");

function findGroupCourseCategoryByProperty(requestObject, callback) {
	var query = groupCourseCategoryMaster.findOne({});
	if (typeof requestObject.categoryId !== "undefined" && requestObject.categoryId !== null)
		query.where("categoryId").equals(requestObject.categoryId);
	if (typeof requestObject.categoryName !== "undefined" && requestObject.categoryName !== null)
		query.where("categoryName").equals(requestObject.categoryName);

	var responseObject = new Object();
	query.exec(function (error, data) {
		if (error) {
			logger.error(error);
			responseObject.responseCode = responseCode.MONGO_ERROR;
			callback(error, responseObject);
			return;
		}
		responseObject.responseCode = responseCode.SUCCESS;
		responseObject.responseData = data;
		callback(null, responseObject);
	});
}

module.exports = findGroupCourseCategoryByProperty;

// Unit Test Case
if (require.main === module) {
	var requestObject = new Object();
	//requestObject.categoryId = "categoryId";
	//requestObject.categoryName = "categoryName";

	findGroupCourseCategoryByProperty(requestObject, function(error, responseObject) {
		console.log("Response Code - " + responseObject.responseCode);
		if (error)
			console.log("Error - " + error);
		else
			console.log("Response Data - " + responseObject.responseData);
	});
}