var groupCourseCategoryMaster = require("../models/group_course_category_master");
var logger = require("../../utils/logger");
var responseCode = require("../../utils/response_code");

function findGroupCourseCategoryList(requestObject, callback) {
	var query = groupCourseCategoryMaster.find({});
	if (typeof requestObject.status !== "undefined" && requestObject.status !== null)
		query.where("status").equals(requestObject.status);
	query.sort("displayOrder");

	var responseObject = new Object();
	query.exec(function (error, data) {
		if (error) {
			logger.error(error);
			responseObject.responseCode = responseCode.MONGO_ERROR;
			callback(error, responseObject);
			return;
		}
		responseObject.responseCode = responseCode.SUCCESS;
		responseObject.responseData = data;
		callback(null, responseObject);
	});
}

module.exports = findGroupCourseCategoryList;

// Unit Test Case
if (require.main === module) {
	var requestObject = new Object();
	//requestObject.status = "ACTIVE";

	findGroupCourseCategoryList(requestObject, function(error, responseObject) {
		console.log("Response Code - " + responseObject.responseCode);
		if (error)
			console.log("Error - " + error);
		else
			console.log("Response Data - " + responseObject.responseData);
	});
}