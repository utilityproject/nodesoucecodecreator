var groupCourseCategoryMaster = require("../models/group_course_category_master");
var logger = require("../../utils/logger");
var responseCode = require("../../utils/response_code");

function updateGroupCourseCategory(requestObject, callback) {
	var updateObject = new Object();
	if(requestObject.courseId)
		updateObject.courseId = requestObject.courseId;
	if(requestObject.categoryName)
		updateObject.categoryName = requestObject.categoryName;
	if(requestObject.categoryType)
		updateObject.categoryType = requestObject.categoryType;
	if(requestObject.refId)
		updateObject.refId = requestObject.refId;
	if(requestObject.displayOrder)
		updateObject.displayOrder = requestObject.displayOrder;
	if(requestObject.status)
		updateObject.status = requestObject.status;
	updateObject.updateAt = new Date();

	var responseObject = new Object();
	var query = {categoryId: requestObject.categoryId};
	groupCourseCategoryMaster.findOneAndUpdate(query, updateObject, function (error, data) {
		if (error) {
			logger.error(error);
			responseObject.responseCode = responseCode.MONGO_ERROR;
			callback(error, responseObject);
			return;
		}
		responseObject.responseCode = responseCode.SUCCESS;
		responseObject.responseData = data;
		callback(null, responseObject);
	});
}

module.exports = updateGroupCourseCategory;

// Unit Test Case
if (require.main === module) {
	var requestObject = new Object();
	requestObject.courseId = "";
	requestObject.categoryName = "";
	requestObject.categoryType = "";
	requestObject.refId = "";
	requestObject.displayOrder = "";
	requestObject.status = "";
	console.log(requestObject);

	updateGroupCourseCategory(requestObject, function(error, responseObject) {
		console.log("Response Code - " + responseObject.responseCode);
		if (error)
			console.log("Error - " + error);
		else
			console.log("Response Data - " + responseObject.responseData);
	});
}