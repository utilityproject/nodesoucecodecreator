var groupCourseCategoryMaster = require("../models/group_course_category_master");
var logger = require("../../utils/logger");
var responseCode = require("../../utils/response_code");

function addGroupCourseCategory(requestObject, callback) {
	var newGroupCourseCategory = new groupCourseCategoryMaster({
		courseId : requestObject.courseId,
		categoryName : requestObject.categoryName,
		categoryType : requestObject.categoryType,
		refId : requestObject.refId,
		displayOrder : requestObject.displayOrder,
		status : requestObject.status,
		createBy : requestObject.createBy
	});

	var responseObject = new Object();
	newGroupCourseCategory.save(function(error, data) {
		if (error) {
			logger.error(error);
			responseObject.responseCode = responseCode.MONGO_ERROR;
			callback(error, responseObject);
			return;
		}
		responseObject.responseCode = responseCode.SUCCESS;
		responseObject.responseData = data;
		callback(null, responseObject);
	});
}

module.exports = addGroupCourseCategory;

// Unit Test Case
if (require.main === module) {
	var requestObject = new Object();
	requestObject.courseId = "";
	requestObject.categoryName = "";
	requestObject.categoryType = "";
	requestObject.refId = "";
	requestObject.displayOrder = "";
	requestObject.status = "";
	requestObject.createBy = "";
	console.log(requestObject);

	addGroupCourseCategory(requestObject, function(error, responseObject) {
		console.log("Response Code - " + responseObject.responseCode);
		if (error)
			console.log("Error - " + error);
		else
			console.log("Response Data - " + responseObject.responseData);
	});
}