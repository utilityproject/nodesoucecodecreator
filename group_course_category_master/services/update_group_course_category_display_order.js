var groupCourseCategoryMaster = require("../models/group_course_category_master");
var logger = require("../../utils/logger");
var responseCode = require("../../utils/response_code");
var async = require(NODE_PATH + "async");

function updateGroupCourseCategoryDisplayOrder(requestObject, callback) {
	var currentRowNumber = 0;
	async.each(requestObject.groupCourseCategoryList, function(currentRow, cb) {
		currentRowNumber++;
		console.log('currentRow - ', currentRow);

		var updateObject = new Object();
		updateObject.displayOrder = currentRowNumber;
		var responseObject = new Object();
		var query = {categoryId : currentRow.categoryId};
		groupCourseCategoryMaster.findOneAndUpdate(query, updateObject, function(error, data) {
			if (error) {
				logger.error(error);
				cb(error);
				return;
			}
			responseObject.responseCode = responseCode.SUCCESS;
			responseObject.responseData = data;
			cb();
		});
	}, function(error) {
		if (error) {
			responseObject.responseCode = responseCode.MONGO_ERROR;
			callback(error, responseObject);
			return;
		}
		responseObject.responseCode = responseCode.SUCCESS;
		callback(null, responseObject);
	});
}

module.exports = updateGroupCourseCategoryDisplayOrder;

// Unit Test Case
if (require.main === module) {
	var groupCourseCategoryList = [ {
		categoryId : 1,
		displayOrder : 1
	}, {
		categoryId : 2,
		displayOrder : 1
	} ];
	var responseObject = new Object();
	requestObject.groupCourseCategoryList = groupCourseCategoryList;
	console.log(requestObject);

	updateGroupCourseCategory(requestObject, function(error, responseObject) {
		console.log("Response Code - " + responseObject.responseCode);
		if (error)
			console.log("Error - " + error);
		else
			console.log("Response Data - " + responseObject.responseData);
	});
}