require("../../utils/mongo_util");
var mongoose = require("mongoose");
var autoIncrement = require("mongoose-auto-increment");

var GroupCourseCategoryMasterSchema = new mongoose.Schema({
	categoryId: {type: Number, unique: true},
	courseId: {type: Number},
	categoryName: {type: String, unique: true},
	categoryType: {type: String, enum: ['EXAMPLE', 'QUESTION', 'FTSPECIAL'], unique: true},
	refId: {type: String, trim: true, index: true, sparse: true, unique: true},
	displayOrder: {type: Number},
	status: {type: String, enum: ["ACTIVE", "INACTIVE"]},
	createBy: Number,
	createAt: {type: Date, default: Date.now},
	updateAt: {type: Date}
});

module.exports = mongoose.model("GroupCourseCategoryMaster", GroupCourseCategoryMasterSchema, "group_course_category_master");

GroupCourseCategoryMasterSchema.plugin(autoIncrement.plugin, {
	model: "GroupCourseCategoryMaster",
	field: "categoryId",
	startAt: 1,
	incrementBy: 1
});