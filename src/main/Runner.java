package main;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.shobhit.tool.ControllerCreator;
import com.shobhit.tool.DataBean;
import com.shobhit.tool.ModelCreator;
import com.shobhit.tool.RoutesCreator;
import com.shobhit.tool.ServiceCreator;

public class Runner {
	private static final String DIR_PATH = "";
	private static final String SEPARATOR = "/";
	private static final String RESPONSE_CODE_PARAM = "respCode";
	private static final String RESPONSE_DATA_PARAM = "respData";

	//private static final String DIR_PATH = "D:\\ZZZ\\";
	//private static final String SEPARATOR = "\\";
	private static final String FOLDER_NAME = "./modules/";
	private static final String JS_EXTENSION = ".js";

	private static class Constants {
		private static final String DIRECTORY_NAME = "directoryName";
		private static final String MODEL_FILE_NAME = "modelFileName";
		private static final String ROUTES_FILE_NAME = "routesFileName";
		private static final String INDEX_FILE_NAME = "indexFileName";
		private static final String ADD_FILE_NAME = "addFileName";
		private static final String FIND_BY_PROPERTY_FILE_NAME = "findByPropertyFileName";
		private static final String FIND_LIST_FILE_NAME = "findListFileName";
		private static final String UPDATE_FILE_NAME = "updateFileName";
		private static final String UPDATE_DISPLAY_ORDER_FILE_NAME = "updateDisplayOrderFileName";

		private static final String LOGGER_NAME = "loggerName";
		private static final String MONGO_FILE = "../../utils/mongo_connection";
		private static final String HAS_MULTIPLE_APPEND = "hasMultipleAppend";

		private static final String MODEL_VAR_NAME = "modelVarName";
		private static final String MODEL_CLASS_NAME = "modelClassName";
		private static final String MONGO_MODEL_NAME = "mongoModelName";
		private static final String MODEL_PARAMETER = "modelParameter";
		private static final String HAS_SEO_DATA = "hasSEOData";
		private static final String HAS_DISPLAY_ORDER = "hasDisplayOrder";
		private static final String HAS_STATUS = "hasStatus";
		private static final String AUTO_INCREMENT_FIELD = "autoIncrementField";
		private static final String ADD_FIELD_PARAMETERS = "addFieldParameters";
		private static final String CONTROLLER_FIND_FIELD_PARAMETERS = "controllerFindFieldParameters";
		private static final String SERVICE_FIND_FIELD_PARAMETERS = "serviceFindFieldParameters";
		private static final String SERVICE_UPDATE_FIELD_PARAMETERS = "serviceUpdateFieldParameters";

		private static final String MODEL_DIR_NAME = SEPARATOR+"models";
		private static final String CONTROLLER_DIR_NAME = SEPARATOR+"controllers";
		private static final String SERVICE_DIR_NAME = SEPARATOR+"services";
	}

	private static void startActivity() throws Exception {
		List<String> fileNames = new ArrayList<>();
		File[] files = new File(FOLDER_NAME).listFiles();
		for (File file : files)
			if (file.isFile() && !file.getName().toUpperCase().contains("IGNORE"))
				fileNames.add(file.getName());

		BufferedReader reader = null;
		Map<String, String> configMap = new HashMap<String, String>();
		String line = null;

		List<String> modelParamList = new ArrayList<>();
		List<DataBean> modelDataList = new ArrayList<DataBean>();
		List<String> addFieldList = new ArrayList<>();
		List<String> controllerFindFieldList = new ArrayList<>();
		List<String> serviceFindFieldList = new ArrayList<>();
		List<String> serviceUpdateFieldList = new ArrayList<>();
		Map<String, String> fileNameMap = new HashMap<String, String>();

		for(String fileName : fileNames) {
			configMap.clear();
			modelParamList.clear();

			reader = new BufferedReader(new FileReader(new File(FOLDER_NAME+fileName)));
			while ((line = reader.readLine()) != null) {
				if(line.contains("=")) {
					String[] data = line.split("=");
					String propertyKey = data[0].trim();
					String propertyValue = data[1].trim();
					
					if(Constants.MODEL_PARAMETER.equals(propertyKey))
						modelParamList.add(propertyValue);
					else
						configMap.put(propertyKey, propertyValue);
				}
			}

			modelDataList.clear();
			addFieldList.clear();
			controllerFindFieldList.clear();
			serviceFindFieldList.clear();
			serviceUpdateFieldList.clear();

			try {
				String directoryName = configMap.get(Constants.DIRECTORY_NAME);
				String modelFileName = configMap.get(Constants.MODEL_FILE_NAME);
				String routesFileName = configMap.get(Constants.ROUTES_FILE_NAME);
				String indexFileName = configMap.get(Constants.INDEX_FILE_NAME);
				String addFileName = configMap.get(Constants.ADD_FILE_NAME);
				String findByPropertyFileName = configMap.get(Constants.FIND_BY_PROPERTY_FILE_NAME);
				String findListFileName = configMap.get(Constants.FIND_LIST_FILE_NAME);
				String updateFileName = configMap.get(Constants.UPDATE_FILE_NAME);
				String updateDisplayOrderFileName = configMap.get(Constants.UPDATE_DISPLAY_ORDER_FILE_NAME);

				String modelVarName = configMap.get(Constants.MODEL_VAR_NAME);
				String modelClassName = configMap.get(Constants.MODEL_CLASS_NAME);
				String mongoModelName = configMap.get(Constants.MONGO_MODEL_NAME);
				String loggerName = configMap.get(Constants.LOGGER_NAME);
				boolean hasMultipleAppend = Boolean.parseBoolean(configMap.get(Constants.HAS_MULTIPLE_APPEND));

				for(String param : modelParamList)
					modelDataList.add(new DataBean(param.split("#")[0], param.split("#")[1], Boolean.parseBoolean(param.split("#")[2])));

				boolean hasSEOData = Boolean.parseBoolean(configMap.get(Constants.HAS_SEO_DATA));
				boolean hasDisplayOrder = Boolean.parseBoolean(configMap.get(Constants.HAS_DISPLAY_ORDER));
				boolean hasStatus = Boolean.parseBoolean(configMap.get(Constants.HAS_STATUS));
				String autoIncrementField = configMap.get(Constants.AUTO_INCREMENT_FIELD);

				for(String addField : configMap.get(Constants.ADD_FIELD_PARAMETERS).split(","))
					addFieldList.add(addField);

				for(String controllerFindField : configMap.get(Constants.CONTROLLER_FIND_FIELD_PARAMETERS).split(","))
					controllerFindFieldList.add(controllerFindField);

				for(String serviceFindField : configMap.get(Constants.SERVICE_FIND_FIELD_PARAMETERS).split(","))
					serviceFindFieldList.add(serviceFindField);

				String updateCriteria = autoIncrementField;
				for(String serviceUpdateField : configMap.get(Constants.SERVICE_UPDATE_FIELD_PARAMETERS).split(","))
					serviceUpdateFieldList.add(serviceUpdateField);

				//	Setting Data Variables Start
				fileNameMap.put(Constants.ADD_FILE_NAME, addFileName);
				fileNameMap.put(Constants.FIND_BY_PROPERTY_FILE_NAME, findByPropertyFileName);
				fileNameMap.put(Constants.FIND_LIST_FILE_NAME, findListFileName);
				fileNameMap.put(Constants.UPDATE_FILE_NAME, updateFileName);
				if(hasDisplayOrder)
					fileNameMap.put(Constants.UPDATE_DISPLAY_ORDER_FILE_NAME, updateDisplayOrderFileName);
				//	Setting Data Variables End

				//	Creating Directory Structure
				File file = new File(DIR_PATH+directoryName);
				if(!file.exists())
					file.mkdir();

				//	Creating Model
				String path = DIR_PATH+directoryName+Constants.MODEL_DIR_NAME;
				file = new File(path);
				if(!file.exists())
					file.mkdir();
				ModelCreator.createModel(path+SEPARATOR+modelFileName+JS_EXTENSION, modelClassName, mongoModelName, modelDataList, hasSEOData, hasDisplayOrder, hasStatus, autoIncrementField,Constants.MONGO_FILE);

				//	Creating Routes
				path = DIR_PATH+directoryName;
				RoutesCreator.createRoutes(path+SEPARATOR+routesFileName+JS_EXTENSION, modelClassName, hasDisplayOrder,hasMultipleAppend);

				//	Creating Controllers
				path = DIR_PATH+directoryName+Constants.CONTROLLER_DIR_NAME;
				file = new File(path);
				if(!file.exists())
					file.mkdir();
				ControllerCreator.createIndexController(path+SEPARATOR+indexFileName+JS_EXTENSION, modelClassName, fileNameMap,hasMultipleAppend);
				ControllerCreator.addController(path+SEPARATOR+addFileName+JS_EXTENSION, modelVarName, modelClassName, addFieldList,loggerName,RESPONSE_CODE_PARAM,RESPONSE_DATA_PARAM);
				ControllerCreator.findByPropertyController(path+SEPARATOR+findByPropertyFileName+JS_EXTENSION, modelVarName, modelClassName, controllerFindFieldList,loggerName,RESPONSE_CODE_PARAM,RESPONSE_DATA_PARAM);
				ControllerCreator.findListController(path+SEPARATOR+findListFileName+JS_EXTENSION, modelVarName, modelClassName, controllerFindFieldList,loggerName,RESPONSE_CODE_PARAM,RESPONSE_DATA_PARAM);
				ControllerCreator.updateController(path+SEPARATOR+updateFileName+JS_EXTENSION, modelVarName, modelClassName, addFieldList,loggerName,RESPONSE_CODE_PARAM,RESPONSE_DATA_PARAM);
				if(hasDisplayOrder)
					ControllerCreator.updateDisplayOrderController(path+SEPARATOR+updateDisplayOrderFileName+JS_EXTENSION, modelVarName, modelClassName,loggerName,RESPONSE_CODE_PARAM,RESPONSE_DATA_PARAM);

				//	Creating Service
				path = DIR_PATH+directoryName+Constants.SERVICE_DIR_NAME;
				file = new File(path);
				if(!file.exists())
					file.mkdir();
				ServiceCreator.createIndexService(path+SEPARATOR+indexFileName+JS_EXTENSION, modelClassName, fileNameMap,hasMultipleAppend);
				ServiceCreator.addService(path+SEPARATOR+addFileName+JS_EXTENSION, modelVarName, modelClassName, modelFileName, addFieldList,loggerName,RESPONSE_CODE_PARAM,RESPONSE_DATA_PARAM);
				ServiceCreator.findByPropertyService(path+SEPARATOR+findByPropertyFileName+JS_EXTENSION, modelVarName, modelClassName, modelFileName, serviceFindFieldList,loggerName,RESPONSE_CODE_PARAM,RESPONSE_DATA_PARAM,controllerFindFieldList);
				ServiceCreator.findListService(path+SEPARATOR+findListFileName+JS_EXTENSION, modelVarName, modelClassName, modelFileName, hasStatus, hasDisplayOrder,loggerName,RESPONSE_CODE_PARAM,RESPONSE_DATA_PARAM,controllerFindFieldList);
				ServiceCreator.updateService(path+SEPARATOR+updateFileName+JS_EXTENSION, modelVarName, modelClassName, modelFileName, updateCriteria, serviceUpdateFieldList,loggerName,RESPONSE_CODE_PARAM,RESPONSE_DATA_PARAM);
				if(hasDisplayOrder)
					ServiceCreator.updateDisplayOrderService(path+SEPARATOR+updateDisplayOrderFileName+JS_EXTENSION, modelVarName, modelClassName, modelFileName, updateCriteria,loggerName,RESPONSE_CODE_PARAM,RESPONSE_DATA_PARAM);
			} catch(Exception ex) {
				ex.printStackTrace();
			}
		}
	}

	public static void main(String[] args) throws Exception {
		Runner.startActivity();
	}
}