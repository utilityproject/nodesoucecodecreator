package com.shobhit.tool;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class RoutesCreator {
	public static void createRoutes(String file, String modelClassName, boolean hasDisplayOrder,boolean hasMultipleAppend) throws IOException {
		StringBuilder routeBuilder = new StringBuilder();
		routeBuilder.append("").append("route.post(\"/add"+modelClassName+"\", obj.add"+modelClassName+");").append("\n")
			.append("").append("route.post(\"/update"+modelClassName+"\", obj.update"+modelClassName+");").append("\n")
			.append("").append("route.post(\"/find"+modelClassName+"ByProperty\", obj.find"+modelClassName+"ByProperty);").append("\n")
			.append("").append("route.post(\"/find"+modelClassName+"List\", obj.find"+modelClassName+"List);").append("\n");

		if(hasDisplayOrder)
			routeBuilder.append("\t").append("route.post(\"/update"+modelClassName+"DisplayOrder\", obj.update"+modelClassName+"DisplayOrder);").append("\n");

		routeBuilder.append("// Routes");

		StringBuilder builder = new StringBuilder();

		if(hasMultipleAppend && new File(file).exists()) {
			StringBuilder preData = new StringBuilder();
			String line = null;
			BufferedReader reader = new BufferedReader(new FileReader(file));
			while((line = reader.readLine()) != null) {
				preData.append(line+"\n");
			}
			preData.deleteCharAt(preData.length()-1);
			reader.close();
			builder.append(preData.toString().replace("// Routes", "\n"+routeBuilder.toString()));
		} else {
			builder.append("var obj = require('./controllers/index');").append("\n")
				.append("var express = require('express');").append("\n")
				.append("var route = express.Router();").append("\n\n")
				.append(routeBuilder).append("\n")
				.append("module.exports = route;").append("\n");
				
		}

		BufferedWriter writer = new BufferedWriter(new FileWriter(new File(file)));
		writer.write(builder.toString());
		writer.flush();
		writer.close();
	}
}